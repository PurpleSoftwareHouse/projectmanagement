/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.malmstein.floatinghints;

public final class R {
	public static final class anim {
		public static final int slide_in_top = 0x7f040000;
		public static final int slide_out_bottom = 0x7f040001;
	}
	public static final class attr {
		public static final int floatingHintInAnimation = 0x7f010002;
		public static final int floatingHintOutAnimation = 0x7f010003;
		public static final int floatingHintSidePadding = 0x7f010001;
		public static final int floatingHintTextAppearance = 0x7f010000;
	}
	public static final class color {
		public static final int font_anthrazit = 0x7f080002;
		public static final int font_grey = 0x7f080000;
		public static final int font_holo_blue = 0x7f080003;
		public static final int font_white = 0x7f080001;
		public static final int font_yellow = 0x7f080004;
	}
	public static final class dimen {
		public static final int activity_horizontal_margin = 0x7f050000;
		public static final int activity_vertical_margin = 0x7f050001;
		public static final int text_s = 0x7f050003;
		public static final int text_xs = 0x7f050002;
	}
	public static final class drawable {
		public static final int ic_launcher = 0x7f02000f;
	}
	public static final class id {
		public static final int action_settings = 0x7f0a007b;
	}
	public static final class layout {
		public static final int activity_main = 0x7f030011;
	}
	public static final class menu {
		public static final int main = 0x7f09000f;
	}
	public static final class string {
		public static final int action_settings = 0x7f060001;
		public static final int app_name = 0x7f060000;
		public static final int hello_world = 0x7f060002;
	}
	public static final class style {
		public static final int AppBaseTheme = 0x7f070000;
		public static final int AppTheme = 0x7f070001;
	}
	public static final class styleable {
		public static final int[] FloatingHintEditTextLayout = { 0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003 };
		public static final int FloatingHintEditTextLayout_floatingHintInAnimation = 2;
		public static final int FloatingHintEditTextLayout_floatingHintOutAnimation = 3;
		public static final int FloatingHintEditTextLayout_floatingHintSidePadding = 1;
		public static final int FloatingHintEditTextLayout_floatingHintTextAppearance = 0;
	}
}
