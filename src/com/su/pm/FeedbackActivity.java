package com.su.pm;

import com.su.pm.util.BaseActivity;

import android.os.Bundle;

public class FeedbackActivity extends BaseActivity {
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);

		setTitleBar(true, true, R.string.feedback);
	}//onCreate

}
