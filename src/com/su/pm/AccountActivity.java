package com.su.pm;

/*
 * @author SuThwe
 * @date 4 April 2016
 * */

import java.util.ArrayList;
import java.util.List;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.su.pm.models.Account;
import com.su.pm.util.BaseActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class AccountActivity extends BaseActivity implements ValidationListener {
	Context context = this;

	TextView txtAmount;
	ListView listAccName;
	Button btnAddAccount;
	
	@NotEmpty
	EditText editAccName, editOpeningBalance, editMinBalance;

	List<Account> accounts;
	List<String> listAccountNames, listAmounts;
	Double opening_balance;
	Dialog dialog;
	CustomList customList;
	
	//Validation
	Validator validator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account);
		
		txtAmount = (TextView) findViewById(R.id.txt_amount);
		listAccName = (ListView) findViewById(R.id.lstAccountName);
		btnAddAccount = (Button) findViewById(R.id.btn_add_account);
		
		setTitleBar(true, true, R.string.account);
		
		//Validation
		validator = new Validator(this);
		validator.setValidationListener((ValidationListener) this);
		
		LoadData();
		
		listAccName.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, AccountDetailActivity.class);
				intent.putExtra("AccountName", listAccountNames.get(pos));
				startActivity(intent);
			}
		});
		
		btnAddAccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				addAccount();
			}
		});
	}//onCreate
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent intent = new Intent(context, MenuActivity.class);
		startActivity(intent);
	}//onBackPressed
	
	private void LoadData() {
		// TODO Auto-generated method stub
		listAccountNames = new ArrayList<String>();
		listAmounts = new ArrayList<String>();
		
		opening_balance = 0.0;
		accounts = Account.getAllAccounts();
		for(Account acc : accounts) {
			listAccountNames.add(acc.acc_name);
			listAmounts.add(String.valueOf(acc.opening_balance));
			//for total amount
			opening_balance += acc.opening_balance;
		}
		
		txtAmount.setText("MMK " + opening_balance);
		if (customList == null) {
			customList = new CustomList(context, R.layout.two_textview_layout, listAccountNames);
			listAccName.setAdapter(customList);
		}
		else {
			customList.notifyDataSetChanged();
		}
		
	}//LoadData

	private void addAccount() { 
		// TODO Auto-generated method stub
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.add_account_dialog);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		
		editAccName = (EditText) dialog.findViewById(R.id.account_name);
		editOpeningBalance = (EditText) dialog.findViewById(R.id.opening_bal);
		editMinBalance = (EditText) dialog.findViewById(R.id.minimum_bal);
		Button btnAdd = (Button) dialog.findViewById(R.id.btn_add);
		
		btnAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validator.validate(); 
			}
		});
		
		dialog.show();
	}//addAccount
	
	private class CustomList extends ArrayAdapter<String> {

		public CustomList(Context context, int resource, List<String> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
		}
		
		@Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflator.inflate(R.layout.two_textview_layout, parent, false);

            TextView tvType = (TextView) row.findViewById(R.id.type);
            TextView tvAmount = (TextView) row.findViewById(R.id.amount);

            tvType.setText(listAccountNames.get(pos));
            tvAmount.setText(listAmounts.get(pos));
            
            return(row);
        }
	}//CustomList
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		dialog.dismiss();
		
		Account account = new Account();
		account.acc_name = editAccName.getText().toString();
		account.opening_balance = Double.valueOf(editOpeningBalance.getText().toString());
		account.min_balance = Double.valueOf(editMinBalance.getText().toString());
		account.save();
		
		listAccountNames.clear();
		LoadData();
		
	}//onValidationSucceeded

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		// TODO Auto-generated method stub
		for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(context);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        }
	}//onValidationFailed
}
