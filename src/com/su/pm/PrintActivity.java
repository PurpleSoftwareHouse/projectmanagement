package com.su.pm;

/*
 * @author SuThwe
 * @date 4 Jun 2016
 * */

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.activeandroid.Cache;
import com.activeandroid.query.Select;
import com.su.pm.models.Borrow;
import com.su.pm.models.Expense;
import com.su.pm.models.Income;
import com.su.pm.models.Lend;
import com.su.pm.util.BaseActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;

public class PrintActivity extends BaseActivity {
	
	Context context = this;
	
	TextView tvIncome;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_print);
		
		tvIncome = (TextView) findViewById(R.id.export_income);
		
		tvIncome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new ExportDatabaseCSVTask().execute();
			}
		});
		
		setTitleBar(true, true, R.string.print);
	}//onCreate
	
    public class ExportDatabaseCSVTask extends AsyncTask<String, Void, Boolean> {
		private final ProgressDialog dialog = new ProgressDialog(PrintActivity.this);
	
		@Override
		protected void onPreExecute() {
		    this.dialog.setMessage("Exporting database...");
		    this.dialog.show();
		}

		protected Boolean doInBackground(final String... args) {
		    File dbFile=getDatabasePath("Application.db");
		    System.out.println(dbFile);  // displays the data base path in your logcat 

		    File exportDir = new File(Environment.getExternalStorageDirectory(), "");        
		    if (!exportDir.exists()) {
		        exportDir.mkdirs();
		    }

		    File file = new File(exportDir, "incomeDB.csv");
		    try {
		    	if (file.createNewFile()){
		            System.out.println("File is created!");
		            System.out.println("myfile.csv "+file.getAbsolutePath());
		        }else{
		            System.out.println("File already exists.");
		        }
	
		        CSVWriter csvWrite = new CSVWriter(new FileWriter(file));

		        //add title
		        String arrIncome[] ={"INCOME", "","", "", "", "", "", "", "", ""};
		        csvWrite.writeNext(arrIncome);
		        
		        String tableName = Cache.getTableInfo(Income.class).getTableName(); 
		        // Query all items without any conditions 
		        String resultRecords = new Select(tableName + ".*"). from(Income.class).toSql(); 
		        // Execute query on the underlying ActiveAndroid SQLite database 
		        Cursor curCSV = Cache.openDatabase().rawQuery(resultRecords, null);
		        csvWrite.writeNext(curCSV.getColumnNames());
	
//		        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS");
		        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
//		        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy",Locale.getDefault());
		        Date date;
		        String column_date = "";
		        while(curCSV.moveToNext()) {
		        	try {
						date = dateFormat.parse(curCSV.getString(3));
//		        		date = new Date(curCSV.getLong(3));
						column_date = dateFormat.format(date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        	
		        	String arrStr[] ={curCSV.getString(0),curCSV.getString(1),curCSV.getString(2),
		        					  column_date,curCSV.getString(4),curCSV.getString(5),
		        					  curCSV.getString(6),curCSV.getString(7),curCSV.getString(8),curCSV.getString(9)};
		        	csvWrite.writeNext(arrStr);
		        }
		        
		        //add for blank line
		        String arrBlank[] ={"", "","", "", "", "", "", "", "", ""};
		        csvWrite.writeNext(arrBlank);
		        csvWrite.writeNext(arrBlank);
		        //add title
		        String arrExpense[] ={"EXPENSE", "","", "", "", "", "", "", "", ""};
		        csvWrite.writeNext(arrExpense);
		        
		        tableName = Cache.getTableInfo(Expense.class).getTableName(); 
		        resultRecords = new Select(tableName + ".*"). from(Expense.class).toSql(); 
		        // Execute query on the underlying ActiveAndroid SQLite database 
		        curCSV = Cache.openDatabase().rawQuery(resultRecords, null);
		        csvWrite.writeNext(curCSV.getColumnNames());
	
		        while(curCSV.moveToNext()) {
		        	try {
						date = dateFormat.parse(curCSV.getString(3));
						column_date = dateFormat.format(date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        	
		        	String arrStr[] ={curCSV.getString(0),curCSV.getString(1),curCSV.getString(2),
		        					  column_date,curCSV.getString(4),curCSV.getString(5),
		        					  curCSV.getString(6),curCSV.getString(7),curCSV.getString(8),curCSV.getString(9)};
		        	csvWrite.writeNext(arrStr);
		        }

		        //add for blank line
		        csvWrite.writeNext(arrBlank);
		        csvWrite.writeNext(arrBlank);
		        //add title
		        String arrLend[] ={"LEND", "","", "", "", "", "", "", "", ""};
		        csvWrite.writeNext(arrLend);
		        
		        tableName = Cache.getTableInfo(Lend.class).getTableName(); 
		        resultRecords = new Select(tableName + ".*"). from(Lend.class).toSql(); 
		        // Execute query on the underlying ActiveAndroid SQLite database 
		        curCSV = Cache.openDatabase().rawQuery(resultRecords, null);
		        csvWrite.writeNext(curCSV.getColumnNames());
	
		        while(curCSV.moveToNext()) {
		        	String arrStr[] ={curCSV.getString(0),curCSV.getString(1),curCSV.getString(2),
		        					  curCSV.getString(3),curCSV.getString(4),curCSV.getString(5),
		        					  curCSV.getString(6),curCSV.getString(7),curCSV.getString(8),
		        					  curCSV.getString(9), curCSV.getString(10)};
		        	csvWrite.writeNext(arrStr);
		        }


		        //add for blank line
		        csvWrite.writeNext(arrBlank);
		        csvWrite.writeNext(arrBlank);
		        //add title
		        String arrBorrow[] ={"BORROW", "","", "", "", "", "", "", "", ""};
		        csvWrite.writeNext(arrBorrow);       
		        
		        tableName = Cache.getTableInfo(Borrow.class).getTableName(); 
		        resultRecords = new Select(tableName + ".*"). from(Borrow.class).toSql(); 
		        // Execute query on the underlying ActiveAndroid SQLite database 
		        curCSV = Cache.openDatabase().rawQuery(resultRecords, null);
		        csvWrite.writeNext(curCSV.getColumnNames());
	
		        while(curCSV.moveToNext()) {
		        	String arrStr[] ={curCSV.getString(0),curCSV.getString(1),curCSV.getString(2),
		        					  curCSV.getString(3),curCSV.getString(4),curCSV.getString(5),
		        					  curCSV.getString(6),curCSV.getString(7),curCSV.getString(8),
		        					  curCSV.getString(9), curCSV.getString(10)};
		        	csvWrite.writeNext(arrStr);
		        }
		        
		        csvWrite.close();
		        curCSV.close();
		        
		        return true;
		        
		    } catch (IOException e) {
		        Log.e("MainActivity", e.getMessage(), e);
		        return false;
		    }
		}
	
		protected void onPostExecute(final Boolean success)	
		{
		    if (this.dialog.isShowing()) {
		        this.dialog.dismiss();
		    }
		    if (success) {
		    	new CSVToExcelConverter().execute();
		        Toast.makeText(PrintActivity.this, "Export succeed", Toast.LENGTH_SHORT).show();
		    }
		    else {
		        Toast.makeText(PrintActivity.this, "Export failed", Toast.LENGTH_SHORT).show();
		    }
		}
		
	}//ExportDatabaseCSVTask
    
    public class CSVToExcelConverter extends AsyncTask<String, Void, Boolean> {
    	private final ProgressDialog dialog = new ProgressDialog(PrintActivity.this);

    	@Override
    	protected void onPreExecute() {
    		this.dialog.setMessage("Exporting to excel...");
    		this.dialog.show();
    	}

    	@SuppressWarnings("rawtypes")
		@Override
    	protected Boolean doInBackground(String... params) {
    	    ArrayList arList=null;
    	    ArrayList al=null;

    	    //File dbFile= new File(getDatabasePath("database_name").toString());
//    	    File dbFile=getDatabasePath("Application.db");
//    	    String yes= dbFile.getAbsolutePath();

    	    String inFilePath = Environment.getExternalStorageDirectory().toString()+"/incomeDB.csv";
    	    String outFilePath = Environment.getExternalStorageDirectory().toString()+"/incomeDB_exec.xls";
    	    String thisLine;
//    	    int count=0;

    	    try {

	    	    FileInputStream fis = new FileInputStream(inFilePath);
	    	    DataInputStream myInput = new DataInputStream(fis);
	    	    int i=0;
	    	    arList = new ArrayList();
	    	    while ((thisLine = myInput.readLine()) != null) {
		    	    al = new ArrayList();
		    	    String strar[] = thisLine.split(",");
		    	    for(int j=0;j<strar.length;j++){
		    	    	al.add(strar[j]);
		    	    }
		    	    arList.add(al);
		    	    System.out.println();
		    	    i++;
	    	    }
	    	} catch (Exception e) {
    	        System.out.println("shit");
    	    }

    	    try {
	    	    HSSFWorkbook hwb = new HSSFWorkbook();
	    	    HSSFSheet sheet = hwb.createSheet("new sheet");
	    	    for(int k=0;k<arList.size();k++) {
		    	    ArrayList ardata = (ArrayList)arList.get(k);
		    	    HSSFRow row = sheet.createRow((short) 0+k);
			    	for(int p=0;p<ardata.size();p++) {
			    	    HSSFCell cell = row.createCell((short) p);
			    	    String data = ardata.get(p).toString();
			    	    if(data.startsWith("=")) {
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    data=data.replaceAll("\"", "");
				    	    data=data.replaceAll("=", "");
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else if(data.startsWith("\"")){
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_STRING);
				    	    cell.setCellValue(data);
			    	    }else{
				    	    data=data.replaceAll("\"", "");
				    	    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				    	    cell.setCellValue(data);
			    	    }
			    	}
			    	System.out.println();
	    	    }
	    	    FileOutputStream fileOut = new FileOutputStream(outFilePath);
	    	    hwb.write(fileOut);
	    	    fileOut.close();
	    	    System.out.println("Your excel file has been generated");
    	    } catch ( Exception ex ) {
    	    	ex.printStackTrace();
    	    } //main method ends
    	    return true;
    	}

    	protected void onPostExecute(final Boolean success) {
    	    if (this.dialog.isShowing()) {
    	        this.dialog.dismiss();
    	    }
    	    if (success) {
    	        Toast.makeText(PrintActivity.this, "file is built!", Toast.LENGTH_LONG).show();
    	    }
    	    else
    	    {
    	        Toast.makeText(PrintActivity.this, "file fail to build", Toast.LENGTH_SHORT).show();
    	    }
    	}
    }//CSVToExcelConverter

}
