package com.su.pm;

/*
 * @author SuThwe
 * @date 27 March 2016
 * */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.su.pm.models.Account;
import com.su.pm.models.Borrow;
import com.su.pm.models.Expense;
import com.su.pm.models.Income;
import com.su.pm.models.Lend;
import com.su.pm.models.Project;
import com.su.pm.models.Transaction;
import com.su.pm.util.BaseActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AddTransactionActivity extends BaseActivity implements ValidationListener{
	Context context = this;
	
	@NotEmpty
	EditText editDate;
	@NotEmpty
	EditText editAmount;
	@NotEmpty
	EditText editPayment;
	EditText editReason;
	Button btnAdd;
	
	Date date;
	Project project;
	String type;
	double credit_amount, add_amount;
	List<String> listPayment; 
	ListView listView;
	ArrayAdapter<String> adapter;
	
	//datePicker         
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener datePicker;
	
	//Validation
	Validator validator;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_transaction);
		
		editDate = (EditText) findViewById(R.id.trans_date);
		editAmount = (EditText) findViewById(R.id.amount);
		editPayment = (EditText) findViewById(R.id.payment);
		editReason = (EditText) findViewById(R.id.reason);
		btnAdd = (Button) findViewById(R.id.btn_add);
		
		project = (Project) getIntent().getSerializableExtra("Project");
		type = project.type;
		
		setTitleBar(true, true, R.string.add_transaction);
		
		//Validation
		validator = new Validator(this);
		validator.setValidationListener((ValidationListener) context);
		
		editDate.setText(getCurrentDate());
		
		editPayment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PaymentDialog();
			}
		});
		
    	editDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
				DatePickerDialog dateDialog = new DatePickerDialog(context,
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
			}
		});
	
    	//Date Listener
		datePicker = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				
				date = myCalendar.getTime();
				String calDate = java.text.DateFormat.getDateInstance().format(date);

				editDate.setText(calDate);
			}
		};
		
		btnAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validator.validate();
			}
		});
	}//onCreate
	
	private void PaymentDialog() {
		// TODO Auto-generated method stub
		listPayment = new ArrayList<String>();
		listPayment.addAll(Account.getAccountName());
		
		final Dialog dialog = new Dialog(AddTransactionActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.list_dialog_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		listView = (ListView) dialog.findViewById(R.id.lst_dialog);
		adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, listPayment);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				editPayment.setText(listPayment.get(pos));
			}
		});
		
		dialog.show();
		
	}//PaymentDialog

	private void Save() {
		// TODO Auto-generated method stub		
		Transaction trans = new Transaction();
		trans.project_id = project.id;
		trans.trans_date = date;
		trans.amount = editAmount.getText().toString();
		trans.payment = editPayment.getText().toString();
		trans.reason = editReason.getText().toString();
		trans.type = type;
		trans.save();
		
		List<Account> accounts = Account.getAllAccounts();
		for (Account acc : accounts) {
			if (acc.acc_name.equals(trans.payment)) {
				if (type.equals("Income") || type.equals("Lend")) {
					acc.opening_balance += Double.valueOf(trans.amount);
				}
				else {
					acc.opening_balance -= Double.valueOf(trans.amount);
				}
				acc.save();
				break;
			}
		}
				
		Double amount = Double.valueOf(editAmount.getText().toString());
		Double paid, credit;
		if (type.equals("Income")) {
			Income income = Income.getIncomeById(project.id);
			paid = Double.valueOf(income.paid);
			credit = Double.valueOf(income.credit);
			income.paid = String.valueOf(paid + amount);
			income.credit = String.valueOf(credit - amount);
			income.save();
			
			project = Income.convertProject(income);
		}
		else if (type.equals("Expense")) {
			Expense expense = Expense.getExpenseById(project.id);
			paid = Double.valueOf(expense.paid);
			credit = Double.valueOf(expense.credit);
			expense.paid = String.valueOf(paid + amount);
			expense.credit = String.valueOf(credit - amount);
			expense.save();

			project = Expense.convertProject(expense);
		}
		else if (type.equals("Lend")) {
			Lend lend = Lend.getLendById(project.id);
			paid = Double.valueOf(lend.paid);
			credit = Double.valueOf(lend.credit);
			lend.paid = String.valueOf(paid + amount);
			lend.credit = String.valueOf(credit - amount);
			lend.save();

			project = Lend.convertProject(lend);
		}
		else if (type.equals("Borrow")) {
			Borrow borrow = Borrow.getBorrowById(project.id);
			paid = Double.valueOf(borrow.paid);
			credit = Double.valueOf(borrow.credit);
			borrow.paid = String.valueOf(paid + amount);
			borrow.credit = String.valueOf(credit - amount);
			borrow.save();

			project = Borrow.convertProject(borrow);
		}
		Toast.makeText(context, R.string.data_saved, Toast.LENGTH_LONG).show();
		
		if (type.equals("Income") || type.equals("Expense")) {
			Intent intent = new Intent(context, InEx_TransactionActivity.class);
			intent.putExtra("Project", project);
			intent.putExtra("Back", "detail");
			startActivity(intent);
		}
		else if (type.equals("Lend") || type.equals("Borrow")) {
			Intent intent = new Intent(context, LeBo_TransactionActivity.class);
			intent.putExtra("Project", project);
			intent.putExtra("Back", "detail");
			startActivity(intent);
		}
		
	}//Save
	
	@SuppressLint("SimpleDateFormat")
	private String getCurrentDate() {
		// TODO Auto-generated method stub
		myCalendar.get(Calendar.YEAR); 
		myCalendar.get(Calendar.MONTH); 
		myCalendar.get(Calendar.DAY_OF_MONTH);

		date = myCalendar.getTime();
		String calDate = java.text.DateFormat.getDateInstance().format(date);
		
		return calDate;
	}
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		credit_amount = Double.valueOf(project.credit);
		add_amount = Double.valueOf(editAmount.getText().toString());
		if (credit_amount < add_amount) {
			editAmount.setError(getResources().getString(R.string.amount_shouldnt_be_greater_than_credit_amount) + project.credit);
		}
		else {
			Save();
		}
		
	}//onValidationSucceeded

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		// TODO Auto-generated method stub
		for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(context);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        }
	}//onValidationFailed

}
