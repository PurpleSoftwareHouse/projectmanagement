package com.su.pm;

/*
 * @author SuThwe
 * @date 22 February 2016
 * */

import java.util.List;
import java.util.Locale;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.su.pm.models.Account;
import com.su.pm.util.BaseActivity;
import com.su.pm.util.CommonConstants;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;

public class LoginActivity extends BaseActivity implements ValidationListener {
	Context context = this;
	
	@NotEmpty
	EditText editUserName;
	@Password(min = 4)
	EditText editPassword;
	Button btnLogin;
	boolean flag;
	
	//Validation
	Validator validator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		editUserName = (EditText) findViewById(R.id.login_email);
		editPassword = (EditText) findViewById(R.id.login_pwd);
		btnLogin = (Button) findViewById(R.id.login_button);
		
		//Validation
		validator = new Validator(this);
		validator.setValidationListener((ValidationListener) LoginActivity.this);		

		flag = getIntent().getBooleanExtra("Refresh", true);
		if (flag) {
			setLocale();
		}
		
		editUserName.setText("Su");
		editPassword.setText("1111");
		
		//save cash, bank as default to account model
		if (pref.getINIT_ACCOUNT()) {
			Account.index();
			pref.setINIT_ACCOUNT(false);
		}
		
		btnLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validator.validate();
			}
		});
	}//onCreate
	
	private void onSuccess() {
		// TODO Auto-generated method stub
		if(pref.getEMAIL().equals("")) {
			pref.setEMAIL(editUserName.getText().toString());
			pref.setPASSWORD(editPassword.getText().toString());
		}
		
//		Toast.makeText(context, R.string.successfully_login, Toast.LENGTH_SHORT).show();
		Intent intent = new Intent(context, MenuActivity.class);
		startActivity(intent);
	}

	public void setLocale() {
		flag = false;// to prevent refresh again
		Locale myLocale;
		if (pref.getLOCALE().equals("")) {
			myLocale = new Locale(CommonConstants.ENGLISH_LOCALE);
		}
		else {
			myLocale = new Locale(pref.getLOCALE());
		}
		
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, LoginActivity.class);
        refresh.putExtra("Refresh", false);
        startActivity(refresh);
        
    }//setLocale
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		onSuccess();
		/*if(connectionManager.isConnected()) {		
			
		}
		else {
			Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
		}*/
	}//onValidationSucceeded

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		// TODO Auto-generated method stub
		for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
	}//onValidationFailed
}
