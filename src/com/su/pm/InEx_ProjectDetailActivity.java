package com.su.pm;

/*
 * @author SuThwe
 * @date 7 March 2016
 * */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.su.pm.models.Expense;
import com.su.pm.models.Income;
import com.su.pm.models.Project;
import com.su.pm.util.BaseActivity;
import com.su.pm.util.Preferences;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class InEx_ProjectDetailActivity extends BaseActivity implements OnItemSelectedListener {
	Context context = this;
	
	TextView txtProjectName;
	TextView txtBalance;
	TextView txtIncome;
	TextView txtExpense;
	TextView spStartDate;
	TextView spEndDate;
	Spinner spTitle;
	Spinner spType;
	ListView listView;
	TextView txtTotalIncome;
	TextView txtTotalExpense;
	TextView txtNoData;
	
	CustomList customList;
	String projectName;
	String balance;
	String income;
	String expense;
	String no_filter_amount;
	Date StartDate = null, EndDate = null;
	String[] stTitle, stPayment;
	TextView txtDate;
	ArrayList<Project> projectList;
	List<String> titleList;
	Preferences pref;
	ArrayAdapter<String> titleAdapter;//for title spinner
	
	//datePicker
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener datePicker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_project_detail);

		listView = (ListView) findViewById(R.id.project_detail_listview);
		setTitleBar(true, true, R.string.in_ex_action_bar_title);  

		pref = Preferences.getInstance(context);
		projectName = pref.getPROJECT_NAME();
		balance = pref.getBALANCE();
		income = pref.getINCOME();
		expense = pref.getEXPENSE();
		titleList = new ArrayList<String>();

		//add header view
		listView.addHeaderView(addHeaderView(), null, false);

		//get Income project when activity start 
		projectList = (ArrayList<Project>) Income.getIncomesAsProject(projectName);
		customList = new CustomList(context, R.layout.in_ex_summary_row, projectList);
		listView.setAdapter(customList);
		
		//if no data, remove listview and show no data text
		if (projectList.size() <= 0) {
			txtNoData.setVisibility(View.VISIBLE);
		}
		else {
			calculateAmount();
		}
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, InEx_TransactionActivity.class);
				intent.putExtra("Project", projectList.get(pos-1));
				intent.putExtra("Back", "detail");
				startActivity(intent);
			}
		});

	   	//Date Listener
		datePicker = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				
				Date date = myCalendar.getTime();
				String calDate = java.text.DateFormat.getDateInstance().format(date);
				
				if (txtDate.getId() == spStartDate.getId()) {
					StartDate = date;
				}
				else if(txtDate.getId() == spEndDate.getId()){
					EndDate = date;
				}

				txtDate.setText(calDate);
			}
		};

	}//onCreate
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
//		Intent intent = new Intent(context, IncomeExpenseActivity.class);
//		intent.putExtra("Summary", "summary");
//		startActivity(intent);
	}

	private ViewGroup addHeaderView() {
		// TODO Auto-generated method stub
		LayoutInflater inflator = getLayoutInflater();
		ViewGroup layout = (ViewGroup) inflator.inflate(R.layout.in_ex_summary_header, listView, false);
		
		txtProjectName = (TextView) layout.findViewById(R.id.project_name);
		txtBalance = (TextView) layout.findViewById(R.id.balance);
		txtIncome = (TextView) layout.findViewById(R.id.income);
		txtExpense = (TextView) layout.findViewById(R.id.expense);
		spStartDate = (TextView) layout.findViewById(R.id.start_date);
		spEndDate = (TextView) layout.findViewById(R.id.end_date);
		spTitle = (Spinner) layout.findViewById(R.id.title);
		spType = (Spinner) layout.findViewById(R.id.type);
		txtTotalIncome = (TextView) layout.findViewById(R.id.total_income);
		txtTotalExpense = (TextView) layout.findViewById(R.id.total_expense);
		txtNoData = (TextView) layout.findViewById(R.id.no_data);

		txtProjectName.setText(projectName);
		txtBalance.setText(getResources().getString(R.string.balance)+ "  " + balance);
		txtIncome.setText(getResources().getString(R.string.income)+ "   " + income);
		txtExpense.setText(getResources().getString(R.string.expense)+ "  " + expense);
		
		addTitlesToSpinner("Income");		
		String[] type = {
							getResources().getString(R.string.income), 
							getResources().getString(R.string.expense) 
						};
		
		ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, type);
		spType.setAdapter(typeAdapter);
		
		spStartDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				txtDate = spStartDate;
				
				DatePickerDialog dateDialog = new DatePickerDialog(context,
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
				
				dateDialog.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						//call filter after select date
						FilterProject();
					}
				});
			}
		});

		spEndDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				txtDate = spEndDate;
				
				DatePickerDialog dateDialog = new DatePickerDialog(context,
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
				
				dateDialog.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						//call filter after select date
						FilterProject();
					}
				});
			}
		});
		
		spTitle.setOnItemSelectedListener(this);
		spType.setOnItemSelectedListener(this);
		
		return layout;
		
	}//addHeaderView 
	
	private void addTitlesToSpinner(String type) {
		// TODO Auto-generated method stub
		titleList.clear();
		if (type.equals("Income")) {
			titleList.addAll(Income.getAllTitles(projectName));
		}
	    else if (type.equals("Expense")) {
			titleList.addAll(Expense.getAllTitles(projectName));
		}
	    titleList.add(0, getResources().getString(R.string.title));
	    
		//add to title
	    if (titleAdapter == null) {
	    	titleAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, titleList);
			spTitle.setAdapter(titleAdapter);
		}
	    else {
	    	titleAdapter.setNotifyOnChange(true);
	    }
	}
	
	private class CustomList extends ArrayAdapter<Project> {
		
		Context context;
		ArrayList<Project> projectList;

		private CustomList(Context c, int resource, ArrayList<Project> projectList) {
			super(c, R.layout.in_ex_summary_row, projectList);
			this.context = c;
			this.projectList = projectList;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflater.inflate(R.layout.in_ex_summary_row, parent, false);
			
			TextView txtDate = (TextView) row.findViewById(R.id.txt_date);
			TextView txtTitle = (TextView) row.findViewById(R.id.title);
			TextView txtSubTitle = (TextView) row.findViewById(R.id.sub_title);
			TextView txtAmount = (TextView) row.findViewById(R.id.amount);
			TextView txtCredit = (TextView) row.findViewById(R.id.credit);
			
			Project project = projectList.get(position);
			String dd = java.text.DateFormat.getDateInstance().format(project.project_date);
			txtDate.setText(dd);
			txtTitle.setText(project.title);
			txtSubTitle.setText(project.sub_title);
			txtAmount.setText(project.amount);
			txtCredit.setText(project.credit);
			
			return row;
		}
		
	}//CustomList
	
	private void FilterProject() {
		// TODO Auto-generated method stub
		String title;
		if(spTitle.getSelectedItemPosition() == 0) {
			title = "";
		}
		else {
			title = spTitle.getSelectedItem().toString();
		}

		List<Project> proList = new ArrayList<Project>();
		if (spType.getSelectedItemPosition() == 0) {
			proList = (ArrayList<Project>) Income.getIncomeByTitle(StartDate, EndDate, title, projectName);
			txtTotalExpense.setText(Expense.getAmountByTitle(StartDate, EndDate, title, projectName));
		}
		else {
			proList = (ArrayList<Project>) Expense.getExpenseByTitle(StartDate, EndDate, title, projectName);
			txtTotalIncome.setText(Income.getAmountByTitle(StartDate, EndDate, title, projectName));
		}
		
		projectList.clear();
		projectList.addAll(proList);
		
		if (projectList.size() <= 0) {
			txtNoData.setVisibility(View.VISIBLE);
			txtTotalIncome.setText("0.0");
			txtTotalExpense.setText("0.0");
		}
		else {
			txtNoData.setVisibility(View.GONE);
			calculateAmount();
		}

		customList.notifyDataSetChanged();

		addTitlesToSpinner(spType.getSelectedItem().toString());
	}

	private void calculateAmount() {
		// TODO Auto-generated method stub
		double paid_amt = 0.0;
		for(Project pro : projectList) {
			paid_amt += Double.valueOf(pro.amount);
		}
		
		if (spType.getSelectedItemPosition() == 0) 
			txtTotalIncome.setText(paid_amt + " MMK");
		else 
			txtTotalExpense.setText(paid_amt + " MMK");
		
	}//calculateAmount

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		FilterProject();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
