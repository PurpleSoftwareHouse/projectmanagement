package com.su.pm;

/*
 * @author SuThwe
 * @date 25 February 2016
 * */

import com.su.pm.adapter.InEx_PagerAdapter;
import com.su.pm.util.BaseFragmentActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

public class IncomeExpenseActivity extends BaseFragmentActivity {
	Context context = this;

	ViewPager viewPager;
	InEx_PagerAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_income_expense);

		viewPager = (ViewPager) findViewById(R.id.view_pager);
		
		setTitleBar(true, true, R.string.income_expense);
		
		//for viewpager
        mAdapter = new InEx_PagerAdapter(getSupportFragmentManager()); 
        viewPager.setAdapter(mAdapter);
		
        String page = getIntent().getStringExtra("Summary");
        if (page != null) {
			viewPager.setCurrentItem(2);
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(context, MenuActivity.class);
		startActivity(intent);
	}

}
