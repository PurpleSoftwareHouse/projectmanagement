package com.su.pm;

/*
 * @author SuThwe
 * @date 24 February 2016
 * */

import com.su.pm.util.BaseActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.content.Context;
import android.content.Intent;

public class MenuActivity extends BaseActivity {
	Context context = this;
	
	Button btnIncomeExpense;
	Button btnLendBorrow;
	Button btnAccount;
	Button btnSetting;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		btnIncomeExpense = (Button) findViewById(R.id.btn_income);
		btnLendBorrow = (Button) findViewById(R.id.btn_lend);
		btnAccount = (Button) findViewById(R.id.account);
		btnSetting = (Button) findViewById(R.id.btn_setting);
		
		setTitleBar(true, false, R.string.menu_action_bar_title);  
		
		btnIncomeExpense.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, IncomeExpenseActivity.class);
				startActivity(intent);
			}
		});
		
		btnLendBorrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, LendBorrowActivity.class);
				startActivity(intent);
			}
		});
		
		btnAccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, AccountActivity.class);
				startActivity(intent);
			}
		});
		
		btnSetting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, SettingsActivity .class);
				startActivity(intent);
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		moveTaskToBack(true);
	}
}
