package com.su.pm.adapter;

/*
 * @author SuThwe
 * @date 26 February 2016
 * */

import com.su.pm.fragment.ExpenseFragment;
import com.su.pm.fragment.InEx_SummaryFragment;
import com.su.pm.fragment.IncomeFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PagerAdapter extends FragmentPagerAdapter {

	private Fragment mFragment;
    private FragmentManager frgManager;
	
    public PagerAdapter(FragmentManager fm) {
        super(fm);
        frgManager = fm;
    }

	@Override
	public Fragment getItem(int i) {
		// TODO Auto-generated method stub
		switch (i) {
			case 0:
	            return new IncomeFragment();
	        case 1:
	            return new ExpenseFragment();
	        case 2:
	            return new InEx_SummaryFragment();
	    }
	    return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		// TODO Auto-generated method stub
		switch (position) {
			case 0:
	            return "Income";
	        case 1:
	            return "Expense";
	        case 2:
	            return "Summary";
	    }
		return null;
	}

}
