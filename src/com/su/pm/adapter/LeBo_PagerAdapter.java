package com.su.pm.adapter;

/*
 * @author SuThwe
 * @date 4 March 2016
 * */

import com.su.pm.fragment.BorrowFragment;
import com.su.pm.fragment.LeBo_SummaryFragment;
import com.su.pm.fragment.LendFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class LeBo_PagerAdapter extends FragmentPagerAdapter {

	private Fragment mFragment;
    private FragmentManager frgManager;
	
    public LeBo_PagerAdapter(FragmentManager fm) {
        super(fm);
        frgManager = fm;
    }

	@Override
	public Fragment getItem(int i) {
		// TODO Auto-generated method stub
		switch (i) {
			case 0:
	            return new LendFragment();
	        case 1:
	            return new BorrowFragment();
	        case 2:
	            return new LeBo_SummaryFragment();
	    }
	    return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		// TODO Auto-generated method stub
		switch (position) {
			case 0:
	            return "Lend";
	        case 1:
	            return "Borrow";
	        case 2:
	            return "Summary";
	    }
		return null;
	}

}
