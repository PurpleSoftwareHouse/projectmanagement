package com.su.pm;

/*
 * @author SuThwe
 * @date 1 May 2016
 * */

import java.util.List;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.su.pm.util.BaseActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePasswordActivity extends BaseActivity implements ValidationListener {
	Context context = this;
	
	@NotEmpty
	EditText editOldPwd;
	@Password
	EditText editNewPwd;
	@ConfirmPassword
	EditText editConfirmPwd;
	Button btnSave;

	//Validation
	Validator validator;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);
		
		editOldPwd = (EditText) findViewById(R.id.old_password);
		editNewPwd = (EditText) findViewById(R.id.new_password);
		editConfirmPwd = (EditText) findViewById(R.id.confirm_password);
		btnSave = (Button) findViewById(R.id.btn_save);
		
		setTitleBar(true, true, R.string.change_password);
		
		//Validation
		validator = new Validator(this);
		validator.setValidationListener((ValidationListener) this);
				
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validator.validate();
			}
		});
		
	}//onCreate

	private void Save() {
		// TODO Auto-generated method stub
		pref.setPASSWORD(editNewPwd.getText().toString());
		Toast.makeText(context, getResources().getString(R.string.password_changed_sucessfully), Toast.LENGTH_LONG).show();
		
		Intent intent = new Intent(context, SettingsActivity.class);
		startActivity(intent);
	}
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		Save();
	}//onValidationSucceeded

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		// TODO Auto-generated method stub
		for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(context);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        }
	}//onValidationFailed
}
