package com.su.pm.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Expense")
public class Expense extends Model {
	@Column(name = "project_name")
	public String project_name;
	@Column(name = "expense_date")
	public Date expense_date;
	@Column(name = "amount")
	public String amount;
	@Column(name = "title")
	public String title;
	@Column(name = "sub_title")
	public String sub_title;
	@Column(name = "payment")
	public String payment;
	@Column(name = "paid")
	public String paid;
	@Column(name = "credit")
	public String credit;
	@Column(name = "reason")
	public String reason;
	
	public Expense() {
		super();
	}

	public Expense(String id, String project_name, Date expense_date, String amount,
			String title, String sub_title, String payment, String paid,
			String credit, String reason) {
		super();
		this.project_name = project_name;
		this.expense_date = expense_date;
		this.amount = amount;
		this.title = title;
		this.sub_title = sub_title;
		this.payment = payment;
		this.paid = paid;
		this.credit = credit;
		this.reason = reason;
	}

	public static List<Expense> getExpenses() {
		// TODO Auto-generated method stub
		List<Expense> expenses =  new Select().from(Expense.class).execute();
	
		return expenses;
	}
	
	public static Expense getExpenseById(Long id) {
		Expense expense =  new Select().from(Expense.class).where("id = ?", id).executeSingle();

        return expense;
    }
	
	public static List<String> getAllTitles(String project_name) {
		// TODO Auto-generated method stub
		List<Expense> expenses =  new Select().distinct().from(Expense.class).where("project_name = ?", project_name).execute();
		
		List<String> titles = new ArrayList<String>();
		for(Expense ex : expenses) {
			titles.add(ex.title);
		}
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(titles);
		titles.clear();
		titles.addAll(hs);
		
		return titles;
	}
    
    public static List<String> getProNames() {
        // TODO Auto-generated method stub
        List<Expense> expenses =  new Select().from(Expense.class).execute();
        
        List<String> list = new ArrayList<String>();
        for(Expense ex : expenses) {
            list.add(ex.project_name);
        }
        
        Set<String> hs = new HashSet<String>();
        hs.addAll(list);
        list.clear();
        list.addAll(hs);
        
        return list;
    }
    
    public static List<String> getTitles() {
        // TODO Auto-generated method stub
        List<Expense> expenses =  new Select().from(Expense.class).execute();
        
        List<String> list = new ArrayList<String>();
        for(Expense ex : expenses) {
            list.add(ex.title);
        }
        
        Set<String> hs = new HashSet<String>();
        hs.addAll(list);
        list.clear();
        list.addAll(hs);
        
        return list;
    }
    
    public static List<String> getSubTitles() {
        // TODO Auto-generated method stub
        List<Expense> expenses =  new Select().from(Expense.class).execute();
        
        List<String> list = new ArrayList<String>();
        for(Expense ex : expenses) {
            list.add(ex.sub_title);
        }
        
        Set<String> hs = new HashSet<String>();
        hs.addAll(list);
        list.clear();
        list.addAll(hs);
        
        return list;
    }
	
	public static List<Project> getExpenseByTitle(Date start_date, Date end_date, String title, String project_name) {
		List<Expense> expenses = new Select()
								.from(Expense.class)
								.where("title like '%"+ title + "%'")
								.where("project_name = ?", project_name)
								.execute();
		
		List<Project> projects = new ArrayList<Project>();
		if (start_date != null && end_date != null) {
			Project pro;
			for(Expense ex : expenses) {
				if (start_date.getTime() <= ex.expense_date.getTime() && ex.expense_date.getTime() < end_date.getTime()) {
					pro = convertProject(ex);
					projects.add(pro);
				}	
			}
		}
		else if (start_date != null) {
			Project pro;
			for(Expense ex : expenses) {
				if (start_date.getTime() <= ex.expense_date.getTime()) {
					pro = convertProject(ex);
					projects.add(pro);
				}	
			}
		}
		else if (end_date != null) {
			Project pro;
			for(Expense ex : expenses) {
				if (ex.expense_date.getTime() <= end_date.getTime()) {
					pro = convertProject(ex);
					projects.add(pro);
				}	
			}
		}
		else {
			Project pro;
			for(Expense ex : expenses) {
				pro = convertProject(ex);
				projects.add(pro);
			}
		}

		return projects;
	}
	
	public static String getAmountByTitle(Date start_date, Date end_date, String title, String project_name) {
		List<Expense> expenses = new Select()
								.from(Expense.class)
								.where("title like '%"+ title + "%'")
								.where("project_name = ?", project_name)
								.execute();
		
		Double amt = 0.0;
		if (start_date != null && end_date != null) {
			for(Expense ex : expenses) {
				if (start_date.getTime() <= ex.expense_date.getTime() && ex.expense_date.getTime() < end_date.getTime()) {
					amt += Double.valueOf(ex.amount);
				}	
			}
		}
		else if (start_date != null) {
			for(Expense ex : expenses) {
				if (start_date.getTime() <= ex.expense_date.getTime()) {
					amt += Double.valueOf(ex.amount);
				}	
			}
		}
		else if (end_date != null) {
			for(Expense ex : expenses) {
				if (ex.expense_date.getTime() <= end_date.getTime()) {
					amt += Double.valueOf(ex.amount);
				}	
			}
		}
		else {
			for(Expense ex : expenses) {
				amt += Double.valueOf(ex.amount);
			}
		}

		return String.valueOf(amt);
	}
	
	public static List<Project> cashExpense() {
		// TODO Auto-generated method stub
		List<Expense> expenses = new Select().from(Expense.class).where("payment = ?", "Cash").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Expense ex : expenses) {
			pro = convertProject(ex);
			projects.add(pro);
		}
		
		return projects;
	}
	
	public static List<Project> bankExpense() {
		// TODO Auto-generated method stub
		List<Expense> expenses = new Select().from(Expense.class).where("payment = ?", "Bank").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Expense ex : expenses) {
			pro = convertProject(ex);
			projects.add(pro);
		}
		
		return projects;
	}

	@SuppressWarnings("deprecation")
    public static List<Project> amountByMonth(String account, int month) {
        List<Expense> expenses =  new Select().from(Expense.class).where("payment = ?", account).execute();
        List<Project> projects = new ArrayList<Project>();
        Date date;
        
        for(Expense ex : expenses) {
            date = ex.expense_date;
            if (date.getMonth() == month) {
                projects.add(convertProject(ex));
            }
        }
        
        return projects;
    }

	/*@SuppressWarnings("deprecation")
	public static Map<String, Project> amountByMonth(String account, int month) {
		List<Expense> expenses =  new Select().from(Expense.class).where("payment = ?", account).execute();
		Map<String, Project> map = new HashMap<String, Project>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		for(Expense ex : expenses) {
			map.put(sdf.format(ex.expense_date), convertProject(ex));
		}
		
		return map;
	}*/
	
	/*@SuppressWarnings("deprecation")
	public static Map<String, String> amountByMonth(String account, int month) {
		List<Expense> expenses =  new Select().from(Expense.class).where("payment = ?", account).execute();
		Map<String, String> map = new HashMap<String, String>();
		String stDate;
		Date date;
		Double amt;
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		for(Expense ex : expenses) {
			amt = 0.0;
			date = ex.expense_date;
			stDate = sdf.format(date);
			
			if (!map.containsKey(stDate)) {
				for(Expense in1 : expenses) {
					if (date.getMonth() == month && date.getDate() == in1.expense_date.getDate()) {
						amt += Double.valueOf(ex.paid);
					}
				}
			}
			
			if (amt != 0.0) {
				map.put(stDate, ""+ amt);
			}
		}
		
		return map;
	}*/

	public static Project convertProject(Expense ex) {
		// TODO Auto-generated method stub
		Project pro = new Project();
		pro.id = ex.getId();
		pro.project_name = ex.project_name;
		pro.project_date = ex.expense_date;
		pro.amount = ex.amount;
		pro.title = ex.title;
		pro.sub_title = ex.sub_title;
		pro.payment = ex.payment;
		pro.paid = ex.paid;
		pro.credit = ex.credit;
		pro.reason = ex.reason;
		pro.type = "Expense";
		
		return pro;
	}
}
