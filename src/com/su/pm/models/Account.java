package com.su.pm.models;

/*
 * @author SuThwe
 * @date 4 April 2016
 * */

import java.util.ArrayList;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Account")
public class Account extends Model {
	@Column(name = "acc_name")
	public String acc_name;
	@Column(name = "opening_balance")
	public double opening_balance;
	@Column(name = "min_balance")
	public double min_balance;
	
	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Account(String acc_name, double opening_balance, double min_balance) {
		super();
		this.acc_name = acc_name;
		this.opening_balance = opening_balance;
		this.min_balance = min_balance;
	}
	
	public static void index() {
		// TODO Auto-generated method stub
		Account cash = new Account("Cash", 0.0, 0.0);
		cash.save();
		
		Account bank = new Account("Bank", 0.0, 0.0);
		bank.save();
	}//index
	
	public static List<Account> getAllAccounts() {
		List<Account> accounts = new Select().from(Account.class).execute();
		return accounts;
	}
	
	public static List<String> getAccountName() {
		List<Account> accounts = new Select().from(Account.class).execute();
		List<String> listAcc = new ArrayList<String>();
		for(Account acc : accounts) {
			listAcc.add(acc.acc_name);
		}
		
		return listAcc;
	}
	
	/*public static getAccountsByName(String acc_name){
		
	}*/
}
