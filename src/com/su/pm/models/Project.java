package com.su.pm.models;

/*
 * @author SuThwe
 * @date 28 February 2016
 * */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.activeandroid.query.Select;

public class Project implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Long id;
	public String project_name;
	public Date project_date;
	public String amount;
	public String title;
	public String sub_title;
	public String payment;
	public String paid;
	public String credit;
	public String reason;
	public String phone;
	public String type;

	public Project() {
		super();
	}

	public Project(Long id, String project_name, Date project_date, String amount,
			String title, String sub_title, String payment, String paid,
			String credit, String reason, String phone, String type) {
		super();
		this.id = id;
		this.project_name = project_name;
		this.project_date = project_date;
		this.amount = amount;
		this.title = title;
		this.sub_title = sub_title;
		this.payment = payment;
		this.paid = paid;
		this.credit = credit;
		this.reason = reason;
		this.phone = phone;
		this.type = type;
	}
	
	public static List<String> getAllProjectTitle(String project_name) {
		// TODO Auto-generated method stub
		List<String> titleList = Income.getAllTitles(project_name);
		titleList.addAll(Expense.getAllTitles(project_name));
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(titleList);
		titleList.clear();
		titleList.addAll(hs);
		
		return titleList;
	}
	
	public static List<Project> getProjects() {
		List<Income> incomes =  new Select().from(Income.class).execute();
		List<Expense> expenses = new Select().from(Expense.class).execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Income in : incomes){
			pro = new Project();
			pro.id = in.getId();
			pro.project_name = in.project_name;
			pro.project_date = in.income_date;
			pro.amount = in.amount;
			pro.title = in.title;
			pro.sub_title = in.sub_title;
			pro.payment = in.payment;
			pro.paid = in.paid;
			pro.credit = in.credit;
			pro.reason = in.reason;
			projects.add(pro);
		}
		
		for(Expense ex : expenses){
			pro = new Project();
			pro.id = ex.getId();
			pro.project_name = ex.project_name;
			pro.project_date = ex.expense_date;
			pro.amount = ex.amount;
			pro.title = ex.title;
			pro.sub_title = ex.sub_title;
			pro.payment = ex.payment;
			pro.paid = ex.paid;
			pro.credit = ex.credit;
			pro.reason = ex.reason;
			projects.add(pro);
		}

		return projects;
	}

}
