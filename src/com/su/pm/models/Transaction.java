package com.su.pm.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Trans")
public class Transaction extends Model {
	@Column(name = "project_id")
	public Long project_id;
	@Column(name = "trans_date")
	public Date trans_date;
	@Column(name = "amount")
	public String amount;
	@Column(name = "payment")
	public String payment;
	@Column(name = "reason")
	public String reason;
	@Column(name = "type")
	public String type;
	
	public Transaction() {
		super();
	}

	public Transaction(Long project_id, Date trans_date, String amount,
			String payment, String reason, String type) {
		super();
		this.project_id = project_id;
		this.trans_date = trans_date;
		this.amount = amount;
		this.payment = payment;
		this.reason = reason;
		this.type = type;
	}
	
	public static List<Transaction> getTransactionById(Long pid, String type) {
		// TODO Auto-generated method stub
		List<Transaction> trans = new Select()
									.from(Transaction.class)
									.where("project_id = ?", pid)
									.where("type = ?", type)
									.execute();
		
		return trans;
	}
	
	public static List<Project> getTransactionAsProjects() {
		// TODO Auto-generated method stub
		List<Transaction> trans = new Select().from(Transaction.class).execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Transaction tran : trans) {
			pro = convertProject(tran);
			projects.add(pro);
		}
		
		return projects;
	}

	public static void transDelete(Long pid, String type) {
		// TODO Auto-generated method stub
		List<Transaction> trans = new Select()
							.from(Transaction.class)
							.where("project_id = ?", pid)
							.where("type = ?", type)
							.execute();
		if (trans != null) {
			for(Transaction tran : trans) {
				tran.delete();
			}
		}
	}
	
	public static Project convertProject(Transaction tran) {
		// TODO Auto-generated method stub
		Project pro = new Project();
		pro.id = tran.getId();
		pro.project_date = tran.trans_date;
		pro.amount = tran.amount;
		pro.payment = tran.payment;
		pro.reason = tran.reason;
		pro.type = tran.type;
		
		if (tran.type.equals("Income")) {
			Income income = Income.getIncomeById(tran.project_id);
			pro.project_name = income.project_name;
			pro.paid = tran.amount;
		}
		else if (tran.type.equals("Expense")) {
			Expense expense = Expense.getExpenseById(tran.project_id);
			pro.project_name = expense.project_name;
			pro.credit = tran.amount;
		}
		
		return pro;
	}//convertProject
}
