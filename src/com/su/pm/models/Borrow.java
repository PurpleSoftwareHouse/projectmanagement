package com.su.pm.models;

/*
 * @author SuThwe
 * @date 6 March 2016
 * */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Borrow")
public class Borrow extends Model {
	
	@Column(name = "borrow_date")
	public Date borrow_date;
	@Column(name = "amount")
	public String amount;
	@Column(name = "rate")
	public String rate;
	@Column(name = "month_or_year")
	public String month_or_year;
	@Column(name = "name")
	public String name;
	@Column(name = "phone")
	public String phone;
	@Column(name = "payment")
	public String payment;
	@Column(name = "paid")
	public String paid;
	@Column(name = "credit")
	public String credit;
	@Column(name = "reason")
	public String reason;

	static SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");

	public Borrow() {
		super();
	}

	public Borrow(Date borrow_date, String amount, String rate, String month_or_year,
			String name, String phone, String payment, String paid, String credit, String reason) {
		super();
		this.borrow_date = borrow_date;
		this.amount = amount;
		this.rate = rate;
		this.month_or_year = month_or_year;
		this.name = name;
		this.phone = phone;
		this.payment = payment;
		this.paid = paid;
		this.credit = credit;
		this.reason = reason;
	}

	public static Borrow getBorrowById(Long id) {
		Borrow borrow =  new Select().from(Borrow.class).where("id = ?", id).executeSingle();

        return borrow;
    }
	
	public double getBorrowAmount() {
		// TODO Auto-generated method stub
		List<Borrow> borrows =  new Select().from(Borrow.class).execute();
		
		double amount = 0.0;
		for(Borrow br : borrows) {
			amount += Double.valueOf(br.amount);
		}
		
		return amount;
	}

	public static List<Project> cashBorrow() {
		// TODO Auto-generated method stub
		List<Borrow> borrows =  new Select().from(Borrow.class).where("payment = ?", "Cash").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Borrow br : borrows) {
			pro = convertProject(br);
			projects.add(pro);
		}
		
		return projects;
	}
	
	public static List<Project> bankBorrow() {
		// TODO Auto-generated method stub
		List<Borrow> borrows =  new Select().from(Borrow.class).where("payment = ?", "Bank").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Borrow br : borrows) {
			pro = convertProject(br);
			projects.add(pro);
		}
		
		return projects;
	}
	
	public static List<String> getNames() {
		// TODO Auto-generated method stub
		List<Borrow> borrows =  new Select().from(Borrow.class).execute();
		
		List<String> list = new ArrayList<String>();
		for(Borrow br : borrows) {
			list.add(br.name);
		}
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(list);
		list.clear();
		list.addAll(hs);
		
		return list;
	}
	
	public static Map<String, String> getPhones() {
		// TODO Auto-generated method stub
		List<Borrow> borrows =  new Select().from(Borrow.class).execute();
		
		Map<String, String> map = new HashMap<String, String>();
		for(Borrow br : borrows) {
			map.put(br.name, br.phone);
		}

		return map;
	}

	public static List<Borrow> getBorrows() {
		List<Borrow> borrows =  new Select().from(Borrow.class).execute();

        return borrows;
    }
	
	public static List<Project> getBorrowByName(Date start_date, Date end_date, String name) {
		List<Borrow> borrows = new Select()
								.from(Borrow.class)
								.where("name = ?", name)
								.execute();
		
		List<Project> projects = new ArrayList<Project>();
		if (start_date != null && end_date != null) {
			Project pro;
			int result, result1;
			for(Borrow bo : borrows) {
				result = sdf.format(start_date).compareTo(sdf.format(bo.borrow_date));
				result1 = sdf.format(end_date).compareTo(sdf.format(bo.borrow_date));
				
				//a < b, -1 
				//a = b, 0
				//a > b, 1
				if ((result < 0 || result == 0) && (result1 > 0 || result1 == 0)) {
					pro = convertProject(bo);
					projects.add(pro);
				}	
			}
		}
		else if (start_date != null) {
			Project pro;
			int result;
			for(Borrow bo : borrows) {
				result = sdf.format(start_date).compareTo(sdf.format(bo.borrow_date));
				
				if (result < 0 || result == 0) {
					pro = convertProject(bo);
					projects.add(pro);
				}	
			}
		}
		else if (end_date != null) {
			Project pro;
			int result1;
			for(Borrow bo : borrows) {
				result1 = sdf.format(end_date).compareTo(sdf.format(bo.borrow_date));
				
				if (result1 > 0 || result1 == 0) {
					pro = convertProject(bo);
					projects.add(pro);
				}	
			}
		}
		else { //both start_date and end_date are null
			Project pro;
			for(Borrow bo : borrows) {
				pro = convertProject(bo);
				projects.add(pro);
			}
		}

		return projects;
	}

    @SuppressWarnings("deprecation")
	public static List<Project> amountByMonth(String account, int month) {
        List<Borrow> borrows =  new Select().from(Borrow.class).where("payment = ?", account).execute();
        List<Project> projects = new ArrayList<Project>();
        Date date;
        
        for(Borrow bo : borrows) {
            date = bo.borrow_date;
            if (date.getMonth() == month) {
                projects.add(convertProject(bo));
            }
        }
        
        return projects;
    }

	/*@SuppressWarnings("deprecation")
	public static Map<String, Project> amountByMonth(String account, int month) {
		List<Borrow> borrows =  new Select().from(Borrow.class).where("payment = ?", account).execute();
		Map<String, Project> map = new HashMap<String, Project>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		for(Borrow bo : borrows) {
			map.put(sdf.format(bo.borrow_date), convertProject(bo));
		}
		
		return map;
	}*/

	/*
	@SuppressWarnings("deprecation")
	public static Map<String, String> amountByMonth(String account, int month) {
		List<Borrow> borrows =  new Select().from(Borrow.class).where("payment = ?", account).execute();
		Map<String, String> map = new HashMap<String, String>();
		String stDate;
		Date date;
		Double amt;
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		for(Borrow bo : borrows) {
			amt = 0.0;
			date = bo.borrow_date;
			stDate = sdf.format(date);
			
			if (!map.containsKey(stDate)) {
				for(Borrow in1 : borrows) {
					if (date.getMonth() == month && date.getDate() == in1.borrow_date.getDate()) {
						amt += Double.valueOf(bo.paid);
					}
				}
			}
			
			if (amt != 0.0) {
				map.put(stDate, ""+ amt);
			}
		}
		
		return map;
	}
*/
	public static Project convertProject(Borrow br) {
		// TODO Auto-generated method stub
		Project pro = new Project();
		pro.id = br.getId();
		pro.project_name = br.name;
		pro.project_date = br.borrow_date;
		pro.amount = br.amount;
		pro.payment = br.payment;
		pro.paid = br.paid;
		pro.credit = br.credit;
		pro.reason = br.reason;
		pro.phone = br.phone;
		pro.type = "Borrow";
		
		return pro;
	}

}
