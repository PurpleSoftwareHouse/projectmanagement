package com.su.pm.models;

/*
 * @author SuThwe
 * @date 6 March 2016
 * */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Lend")
public class Lend extends Model {
	
	@Column(name = "lend_date")
	public Date lend_date;
	@Column(name = "amount")
	public String amount;
	@Column(name = "rate")
	public String rate;
	@Column(name = "month_or_year")
	public String month_or_year;
	@Column(name = "name")
	public String name;
	@Column(name = "phone")
	public String phone;
	@Column(name = "payment")
	public String payment;
	@Column(name = "paid")
	public String paid;
	@Column(name = "credit")
	public String credit;
	@Column(name = "reason")
	public String reason;

	static SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");

	public Lend() {
		super();
	}

	public Lend(Date lend_date, String amount, String rate, String month_or_year,
			String name, String phone, String payment, String paid, String credit, String reason) {
		super();
		this.lend_date = lend_date;
		this.amount = amount;
		this.rate = rate;
		this.month_or_year = month_or_year;
		this.name = name;
		this.phone = phone;
		this.payment = payment;
		this.paid = paid;
		this.credit = credit;
		this.reason = reason;
	}

	public static Lend getLendById(Long id) {
		Lend lend =  new Select().from(Lend.class).where("id = ?", id).executeSingle();

        return lend;
    }
	public double getLendAmount() {
		// TODO Auto-generated method stub
		List<Lend> lends =  new Select().from(Lend.class).execute();
		
		double amount = 0.0;
		for(Lend le : lends) {
			amount += Double.valueOf(le.amount);
		}
		
		return amount;
	}//getLendAmount
	
	public static List<Lend> cashLoan() {
		// TODO Auto-generated method stub
		List<Lend> lends =  new Select().from(Lend.class).where("payment = ?", "Cash").execute();
		return lends;
	}
	
	public static List<Lend> bankLoan() {
		// TODO Auto-generated method stub
		List<Lend> lends =  new Select().from(Lend.class).where("payment = ?", "Bank").execute();
		return lends;
	}
	
	public static List<Project> cashLend() {
		// TODO Auto-generated method stub
		List<Lend> lends =  new Select().from(Lend.class).where("payment = ?", "Cash").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Lend le : lends) {
			pro = convertProject(le);
			projects.add(pro);
		}
		
		return projects;
	}
	
	public static List<Project> bankLend() {
		// TODO Auto-generated method stub
		List<Lend> lends =  new Select().from(Lend.class).where("payment = ?", "Bank").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Lend le : lends) {
			pro = convertProject(le);
			projects.add(pro);
		}
		
		return projects;
	}
	
	public static List<String> getNames() {
		// TODO Auto-generated method stub
		List<Lend> lends =  new Select().from(Lend.class).execute();
		
		List<String> list = new ArrayList<String>();
		for(Lend le : lends) {
			list.add(le.name);
		}
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(list);
		list.clear();
		list.addAll(hs);
		
		return list;
	}
	
	public static Map<String, String> getPhones() {
		// TODO Auto-generated method stub
		List<Lend> lends =  new Select().from(Lend.class).execute();
		
		Map<String, String> map = new HashMap<String, String>();
		for(Lend le : lends) {
			map.put(le.name, le.phone);
		}

		return map;
	}
	
	public static List<Lend> getLends() {
		List<Lend> lends =  new Select().from(Lend.class).execute();

        return lends;
    }
	
	public static List<Project> getLendByName(Date start_date, Date end_date, String name) {
		List<Lend> lends = new Select()
								.from(Lend.class)
								.where("name = ?", name)
								.execute();
		
		List<Project> projects = new ArrayList<Project>();
		if (start_date != null && end_date != null) {
			Project pro;
			int result, result1;
			for(Lend le : lends) {
				result = sdf.format(start_date).compareTo(sdf.format(le.lend_date));
				result1 = sdf.format(end_date).compareTo(sdf.format(le.lend_date));
				
				//a < b, -1 
				//a = b, 0
				//a > b, 1
				if ((result < 0 || result == 0) && (result1 > 0 || result1 == 0)) {
					pro = convertProject(le);
					projects.add(pro);
				}	
			}
		}
		else if (start_date != null) {
			Project pro;
			int result;
			for(Lend le : lends) {
				result = sdf.format(start_date).compareTo(sdf.format(le.lend_date));
				
				if (result < 0 || result == 0) {
					pro = convertProject(le);
					projects.add(pro);
				}	
			}
		}
		else if (end_date != null) {
			Project pro;
			int result1;
			for(Lend le : lends) {
				result1 = sdf.format(end_date).compareTo(sdf.format(le.lend_date));
				
				if (result1 > 0 || result1 == 0) {
					pro = convertProject(le);
					projects.add(pro);
				}	
			}
		}
		else { //both start_date and end_date are null
			Project pro;
			for(Lend le : lends) {
				pro = convertProject(le);
				projects.add(pro);
			}
		}

		return projects;
	}

	@SuppressWarnings("deprecation")
    public static List<Project> amountByMonth(String account, int month) {
        List<Lend> lends =  new Select().from(Lend.class).where("payment = ?", account).execute();
        List<Project> projects = new ArrayList<Project>();
        Date date;
        
        for(Lend le : lends) {
            date = le.lend_date;
            if (date.getMonth() == month) {
                projects.add(convertProject(le));
            }
        }
        
        return projects;
    }

	/*@SuppressWarnings("deprecation")
	public static Map<String, Project> amountByMonth(String account, int month) {
		List<Lend> lends =  new Select().from(Lend.class).where("payment = ?", account).execute();
		Map<String, Project> map = new HashMap<String, Project>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		for(Lend le : lends) {
			map.put(sdf.format(le.lend_date), convertProject(le));
		}
		
		return map;
	}*/

	/*
	@SuppressWarnings("deprecation")
	public static Map<String, String> amountByMonth(String account, int month) {
		List<Lend> lends =  new Select().from(Lend.class).where("payment = ?", account).execute();
		Map<String, String> map = new HashMap<String, String>();
		String stDate;
		Date date;
		Double amt;
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		for(Lend le : lends) {
			amt = 0.0;
			date = le.lend_date;
			stDate = sdf.format(date);
			
			if (!map.containsKey(stDate)) {
				for(Lend in1 : lends) {
					if (date.getMonth() == month && date.getDate() == in1.lend_date.getDate()) {
						amt += Double.valueOf(le.paid);
					}
				}
			}
			
			if (amt != 0.0) {
				map.put(stDate, ""+ amt);
			}
		}
		
		return map;
	}
*/
	public static Project convertProject(Lend le) {
		// TODO Auto-generated method stub
		Project pro = new Project();
		pro.id = le.getId();
		pro.project_name = le.name;
		pro.project_date = le.lend_date;
		pro.amount = le.amount;
		pro.payment = le.payment;
		pro.paid = le.paid;
		pro.credit = le.credit;
		pro.reason = le.reason;
		pro.phone = le.phone;
		pro.type = "Lend";
		
		return pro;
	}

}
