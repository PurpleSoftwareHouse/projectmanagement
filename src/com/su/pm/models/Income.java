package com.su.pm.models;

/*
 * @author SuThwe
 * @date 28 February 2016
 * */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.annotation.SuppressLint;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@SuppressLint("SimpleDateFormat")
@Table(name = "Income")
public class Income extends Model {
	@Column(name = "project_name")
	public String project_name;
	@Column(name = "income_date")
	public Date income_date;
	@Column(name = "amount")
	public String amount;
	@Column(name = "title")
	public String title;
	@Column(name = "sub_title")
	public String sub_title;
	@Column(name = "payment")
	public String payment;
	@Column(name = "paid")
	public String paid;
	@Column(name = "credit")
	public String credit;
	@Column(name = "reason")
	public String reason;
	
	static SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");

	public Income() {
		super();
	}

	public Income(String id, String project_name, Date income_date, String amount,
			String title, String sub_title, String payment, String paid,
			String credit, String reason) {
		super();
		this.project_name = project_name;
		this.income_date = income_date;
		this.amount = amount;
		this.title = title;
		this.sub_title = sub_title;
		this.payment = payment;
		this.paid = paid;
		this.credit = credit;
		this.reason = reason;
	}
	
	public static List<Income> getIncomes() {
		List<Income> incomes =  new Select().from(Income.class).execute();

        return incomes;
    }
	
	public static Income getIncomeById(Long id) {
		Income income =  new Select().from(Income.class).where("id = ?", id).executeSingle();

        return income;
    }
	
	public static List<String> getAllTitles(String project_name) {
		// TODO Auto-generated method stub
		List<Income> incomes =  new Select().from(Income.class).where("project_name = ?", project_name).execute();
		
		List<String> titles = new ArrayList<String>();
		for(Income in : incomes) {
			titles.add(in.title);
		}
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(titles);
		titles.clear();
		titles.addAll(hs);
		
		return titles;
	}
	
	public static List<String> getProNames() {
		// TODO Auto-generated method stub
		List<Income> incomes =  new Select().from(Income.class).execute();
		
		List<String> list = new ArrayList<String>();
		for(Income in : incomes) {
			list.add(in.project_name);
		}
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(list);
		list.clear();
		list.addAll(hs);
		
		return list;
	}
	
	public static List<String> getTitles() {
		// TODO Auto-generated method stub
		List<Income> incomes =  new Select().from(Income.class).execute();
		
		List<String> list = new ArrayList<String>();
		for(Income in : incomes) {
			list.add(in.title);
		}
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(list);
		list.clear();
		list.addAll(hs);
		
		return list;
	}
	
	public static List<String> getSubTitles() {
		// TODO Auto-generated method stub
		List<Income> incomes =  new Select().from(Income.class).execute();
		
		List<String> list = new ArrayList<String>();
		for(Income in : incomes) {
			list.add(in.sub_title);
		}
		
		Set<String> hs = new HashSet<String>();
		hs.addAll(list);
		list.clear();
		list.addAll(hs);
		
		return list;
	}
	
	public static List<Project> getIncomesAsProject(String project_name) {
		// TODO Auto-generated method stub
		List<Income> incomes =  new Select().from(Income.class).where("project_name = ?", project_name).execute();
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		
		for(Income in : incomes) {
			if (in.project_name.equals(project_name)) {
				pro = convertProject(in);
				projects.add(pro);
			}
		}
		
		return projects;
	}
	
	public static List<Project> getIncomeByTitle(Date start_date, Date end_date, String title, String project_name) {
		List<Income> incomes = new Select()
								.from(Income.class)
								.where("title like '%"+ title + "%'")
								.where("project_name = ?", project_name)
								.execute();
		
		List<Project> projects = new ArrayList<Project>();
		if (start_date != null && end_date != null) {
			Project pro;
			int result, result1;
			for(Income in : incomes) {
				result = sdf.format(start_date).compareTo(sdf.format(in.income_date));
				result1 = sdf.format(end_date).compareTo(sdf.format(in.income_date));
				
				//a < b, -1 
				//a = b, 0
				//a > b, 1
				if ((result < 0 || result == 0) && (result1 > 0 || result1 == 0)) {
					pro = convertProject(in);
					projects.add(pro);
				}	
			}
		}
		else if (start_date != null) {
			Project pro;
			int result;
			for(Income in : incomes) {
				result = sdf.format(start_date).compareTo(sdf.format(in.income_date));
				
				if (result < 0 || result == 0) {
					pro = convertProject(in);
					projects.add(pro);
				}	
			}
		}
		else if (end_date != null) {
			Project pro;
			int result1;
			for(Income in : incomes) {
				result1 = sdf.format(end_date).compareTo(sdf.format(in.income_date));
				
				if (result1 > 0 || result1 == 0) {
					pro = convertProject(in);
					projects.add(pro);
				}	
			}
		}
		else { //both start_date and end_date are null
			Project pro;
			for(Income in : incomes) {
				pro = convertProject(in);
				projects.add(pro);
			}
		}

		return projects;
	}
	
	public static String getAmountByTitle(Date start_date, Date end_date, String title, String project_name) {
		List<Income> incomes = new Select()
								.from(Income.class)
								.where("title like '%"+ title + "%'")
								.where("project_name = ?", project_name)
								.execute();

		Double amt = 0.0;
		if (start_date != null && end_date != null) {
			int result, result1;
			for(Income in : incomes) {
				result = sdf.format(start_date).compareTo(sdf.format(in.income_date));
				result1 = sdf.format(end_date).compareTo(sdf.format(in.income_date));
				
				//a < b, -1 
				//a = b, 0
				//a > b, 1
				if ((result < 0 || result == 0) && (result1 > 0 || result1 == 0)) {
					amt += Double.valueOf(in.amount);
				}	
			}
		}
		else if (start_date != null) {
			int result;
			for(Income in : incomes) {
				result = sdf.format(start_date).compareTo(sdf.format(in.income_date));
				
				if (result < 0 || result == 0) {
					amt += Double.valueOf(in.amount);
				}	
			}
		}
		else if (end_date != null) {
			int result1;
			for(Income in : incomes) {
				result1 = sdf.format(end_date).compareTo(sdf.format(in.income_date));
				
				if (result1 > 0 || result1 == 0) {
					amt += Double.valueOf(in.amount);
				}	
			}
		}
		else { //both start_date and end_date are null
			for(Income in : incomes) {
				amt += Double.valueOf(in.amount);
			}
		}

		return String.valueOf(amt);
	}
	
	public static List<Project> cashIncome() {
		// TODO Auto-generated method stub
		List<Income> incomes =  new Select().from(Income.class).where("payment = ?", "Cash").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Income in : incomes) {
			pro = convertProject(in);
			projects.add(pro);
		}
		
		return projects;
	}
	
	public static List<Project> bankIncome() {
		// TODO Auto-generated method stub
		List<Income> incomes =  new Select().from(Income.class).where("payment = ?", "Bank").execute();
		
		List<Project> projects = new ArrayList<Project>();
		Project pro;
		for(Income in : incomes) {
			pro = convertProject(in);
			projects.add(pro);
		}
		
		return projects;
	}

	@SuppressWarnings("deprecation")
	public static List<Project> amountByMonth(String account, int month) {
		List<Income> incomes =  new Select().from(Income.class).where("payment = ?", account).execute();
		List<Project> projects = new ArrayList<Project>();
		Date date;
		
		for(Income in : incomes) {
			date = in.income_date;
			if (date.getMonth() == month) {
				projects.add(convertProject(in));
			}
		}
		
		return projects;
	}

/*
	public static Map<String, Project> amountByMonth(String account, int month) {
		List<Income> incomes =  new Select().from(Income.class).where("payment = ?", account).execute();
		Map<String, Project> map = new HashMap<String, Project>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		
		for(Income in : incomes) {
			map.put(sdf.format(in.income_date), convertProject(in));
		}
		
		return map;
	}
*/
	/*@SuppressWarnings("deprecation")
	public static Map<String, String> amountByMonth(String account, int month) {
		List<Income> incomes =  new Select().from(Income.class).where("payment = ?", account).execute();
		Map<String, String> map = new HashMap<String, String>();
		String stDate;
		Date date;
		Double amt;
		SimpleDateFormat sdf = new SimpleDateFormat("dd EEEE");
		for(Income in : incomes) {
			amt = 0.0;
			date = in.income_date;
			stDate = sdf.format(date);
			
			if (!map.containsKey(stDate)) {
				for(Income in1 : incomes) {
					if (date.getMonth() == month && date.getDate() == in1.income_date.getDate()) {
						amt += Double.valueOf(in.paid);
					}
				}
			}
			
			if (amt != 0.0) {
				map.put(stDate, ""+ amt);
			}
		}
		
		return map;
	}*/
	
	public static Project convertProject(Income in) {
		// TODO Auto-generated method stub
		Project pro = new Project();
		pro.id = in.getId();
		pro.project_name = in.project_name;
		pro.project_date = in.income_date;
		pro.amount = in.amount;
		pro.title = in.title;
		pro.sub_title = in.sub_title;
		pro.payment = in.payment;
		pro.paid = in.paid;
		pro.credit = in.credit;
		pro.reason = in.reason;
		pro.type = "Income";
		
		return pro;
	}//convertProject
}
