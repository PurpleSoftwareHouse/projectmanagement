package com.su.pm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.su.pm.models.Borrow;
import com.su.pm.models.Expense;
import com.su.pm.models.Income;
import com.su.pm.models.Lend;
import com.su.pm.models.Project;
import com.su.pm.util.BaseActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AccountDetailActivity extends BaseActivity {
	
	TextView txtAccName;
	TextView txtTotal;
	TextView txtMonth;
	ImageView imgLeft;
	ImageView imgRight;
	ListView listAccount;
	LinearLayout amountLayout;
	TextView no_data;
	
	String acc_name;
	Map<String, Project> map;
	List<Project> projects;
	ArrayList<String> dates;
	CustomList customList;
	
	//datePicker
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener datePicker;
	String calDate;
	Date date;
	SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
	SimpleDateFormat sdf1 = new SimpleDateFormat("dd EEEE");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_detail);
		
		txtAccName = (TextView) findViewById(R.id.account_name);
		txtTotal = (TextView) findViewById(R.id.total_amount);
		txtMonth = (TextView) findViewById(R.id.month);
		imgLeft = (ImageView) findViewById(R.id.left);
		imgRight = (ImageView) findViewById(R.id.right);
		listAccount = (ListView) findViewById(R.id.list_account);
		no_data = (TextView) findViewById(R.id.no_data);
		
		acc_name = getIntent().getStringExtra("AccountName");
		txtAccName.setText(acc_name);
		setTitle(true, true, acc_name);
		
		dates = new ArrayList<String>();
		map = new HashMap<String, Project>();
		getCurrentDate();
		LoadData();

		imgLeft.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				movePreviousMonth(date);
				LoadData();
			}
		});
		
		imgRight.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				moveNextMonth(date);
				LoadData();
			}
		});
		
	}//onCreate
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent intent = new Intent(context, AccountActivity.class);
		startActivity(intent);
	}//onBackPressed
	
	@SuppressWarnings("deprecation")
	private void LoadData() {
		// TODO Auto-generated method stub
		projects = Income.amountByMonth(acc_name, date.getMonth());
		projects.addAll(Expense.amountByMonth(acc_name, date.getMonth()));
		projects.addAll(Lend.amountByMonth(acc_name, date.getMonth()));
		projects.addAll(Borrow.amountByMonth(acc_name, date.getMonth()));
		
		dates.clear();
		String key;
		for(Project pro : projects) {
			key = pro.type + pro.id;
			map.put(key, pro);
			dates.add(sdf1.format(pro.project_date));
		}

		//remove duplicate date
		Set<String> hs = new HashSet<String>();
		hs.addAll(dates);
		dates.clear();
		dates.addAll(hs);
		
		Collections.sort(dates, new Comparator<String>() {

			@Override
			public int compare(String date1, String date2) {
				// TODO Auto-generated method stub
				return date1.compareTo(date2);
			}
		});

		no_data.setVisibility(View.GONE);
		if (customList == null) {
			customList = new CustomList(context, R.layout.account_listview, dates);
			listAccount.setAdapter(customList);
		}
		else {
			customList.notifyDataSetChanged(); 
		}
		
		if (dates.size() > 0) {
			no_data.setVisibility(View.GONE);
		}
		else {
			no_data.setVisibility(View.VISIBLE);
		}
		
	}//LoadData

	@SuppressLint("SimpleDateFormat")
	private String getCurrentDate() {
		// TODO Auto-generated method stub
		myCalendar.get(Calendar.YEAR); 
		myCalendar.get(Calendar.MONTH); 
		myCalendar.get(Calendar.DAY_OF_MONTH);

		date = myCalendar.getTime();
		calDate = sdf.format(date);
		txtMonth.setText(calDate);
		
		return calDate;
	}//getCurrentDate
	
	protected void moveNextMonth(Date dd) {
		// TODO Auto-generated method stub
		myCalendar.add(Calendar.MONTH, +1);

		date = myCalendar.getTime();
		calDate = sdf.format(date);
		txtMonth.setText(calDate);
	}//moveNextMonth

	protected void movePreviousMonth(Date dd) {
		// TODO Auto-generated method stub
		myCalendar.add(Calendar.MONTH, -1);

		date = myCalendar.getTime();
		calDate = sdf.format(date);
		txtMonth.setText(calDate);
	}//movePreviousMonth
	
	private class CustomList extends ArrayAdapter<String> {

		public CustomList(Context context, int resource, List<String> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
		}
		
		@Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflator.inflate(R.layout.account_listview, parent, false);
            
            TextView tvDate = (TextView) row.findViewById(R.id.tv_date);
            amountLayout = (LinearLayout) row.findViewById(R.id.date_amount);

            tvDate.setText(dates.get(pos));
            for(Project p : projects) {
            	if (dates.get(pos).equals(sdf1.format(p.project_date))) {
					addAmount(p.type, p.paid, p.type+p.id);
				}
            }
            
            return(row);
        }
	}//CustomList
	
	private void addAmount(String type, String amount, String tag) {
		// TODO Auto-generated method stub
		LayoutInflater inflator = getLayoutInflater();
		ViewGroup layout = (ViewGroup) inflator.inflate(R.layout.two_textview_arrow_layout, amountLayout, false);
		TextView tvType = (TextView) layout.findViewById(R.id.type);
		TextView tvAmount = (TextView) layout.findViewById(R.id.amount);
		
		tvType.setText(type);
		tvAmount.setText(amount);
		layout.setTag(tag);
		
		layout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (v.getTag().toString().contains("Income") || v.getTag().toString().contains("Expense")) {
					Intent intent = new Intent(context, InEx_TransactionActivity.class);
					intent.putExtra("Project", map.get(v.getTag().toString()));
					intent.putExtra("Back", "account");
					startActivity(intent);
				}
				else {
					Intent intent = new Intent(context, LeBo_TransactionActivity.class);
					intent.putExtra("Project", map.get(v.getTag().toString()));
					intent.putExtra("Back", "account");
					startActivity(intent);
				}
			}
		});
		
		amountLayout.addView(layout);
	}//addAmount

}
