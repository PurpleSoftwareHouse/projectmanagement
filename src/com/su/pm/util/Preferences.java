package com.su.pm.util;

/*
 * @author SuThwe
 * @date 23 February 2016
 * */

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {
	private Preferences pref;
	private SharedPreferences mPreference;
	private SharedPreferences.Editor mEditor;
	private Context mContext;
	
	private String EMAIL="email";
	private String PASSWORD = "password";
	private String PROJECT_NAME = "project_name";
	private String BALANCE="balance";
	private String INCOME = "income";
	private String EXPENSE = "expense";
	private String LEND = "lend";
	private String BORROW = "borrow";
	private String INIT_ACCOUNT = "init_account";
	private String LOCALE = "locale";
	
	public Preferences(Context context) {
	    this.mContext = context;
	    mPreference = context.getSharedPreferences("KnightFrank", 0);
	    mEditor = mPreference.edit();
	}
	  
	public static Preferences getInstance(Context context){
		Preferences instance = new Preferences(context);
	    instance.mContext = context;
	    return  instance;
	}
	
	public String getEMAIL() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(EMAIL, "");
	}

	public void setEMAIL(String eMAIL) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(EMAIL, eMAIL).commit();
	}

	public String getPASSWORD() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(PASSWORD, "");
	}

	public void setPASSWORD(String pASSWORD) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(PASSWORD, pASSWORD).commit();
	}

	public String getPROJECT_NAME() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(PROJECT_NAME, "");
	}

	public void setPROJECT_NAME(String pROJECT_NAME) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(PROJECT_NAME, pROJECT_NAME).commit();
	}

	public String getBALANCE() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(BALANCE, "");
	}

	public void setBALANCE(String bALANCE) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(BALANCE, bALANCE).commit();
	}

	public String getINCOME() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(INCOME, "");
	}

	public void setINCOME(String iNCOME) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(INCOME, iNCOME).commit();
	}

	public String getEXPENSE() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(EXPENSE, "");
	}

	public void setEXPENSE(String eXPENSE) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(EXPENSE, eXPENSE).commit();
	}

	public String getLEND() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(LEND, "");
	}

	public void setLEND(String lEND) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(LEND, lEND).commit();
	}

	public String getLOCALE() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(LOCALE, "");
	}

	public void setLOCALE(String lOCALE) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(LOCALE, lOCALE).commit();
	}

	public boolean getINIT_ACCOUNT() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getBoolean(INIT_ACCOUNT, true);
	}

	public void setINIT_ACCOUNT(boolean iNIT_ACCOUNT) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putBoolean(INIT_ACCOUNT, iNIT_ACCOUNT).commit();
	}

	public String getBORROW() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		return prefs.getString(BORROW, "");
	}

	public void setBORROW(String bORROW) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		prefs.edit().putString(BORROW, bORROW).commit();
	}

	
	
	
}
