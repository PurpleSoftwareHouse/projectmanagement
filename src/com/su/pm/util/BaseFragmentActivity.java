package com.su.pm.util;

/*
 * @author SuThwe
 * @date 23 February 2016
 * */

import com.su.pm.MenuActivity;
import com.su.pm.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseFragmentActivity extends FragmentActivity {
	public Preferences pref;
	public ConnectionManager connectionManager;
	public Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		context = getApplicationContext();
		
		pref = Preferences.getInstance(context);
		
		connectionManager = new ConnectionManager(context);
	}//onCreate
	
	public void setTitleBar(boolean home, boolean back, int title) {
		TextView titleText = (TextView) findViewById(R.id.title_text);
		ImageView titleHome = (ImageView) findViewById(R.id.title_home);
		ImageView titleBack = (ImageView) findViewById(R.id.title_back);
		
		titleText.setText(title);
		
		if (home) {
			titleHome.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, MenuActivity.class);
					startActivity(intent);
				}
			});
		}
		else {
			titleHome.setVisibility(View.GONE);
		}
		
		if (back) {
			titleBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
		}
		else {
			titleBack.setVisibility(View.GONE);
		}

	}//setTitleBar
}
