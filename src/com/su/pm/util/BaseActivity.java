package com.su.pm.util;

/*
 * @author SuThwe
 * @date 23 February 2016
 * */

import java.util.Locale;

import com.su.pm.MenuActivity;
import com.su.pm.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseActivity extends Activity {
	public Preferences pref;
	public ConnectionManager connectionManager;
	public Context context;
	boolean flag;
	public ImageView titleBack;
	Locale myLocale;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		context = getApplicationContext();
		
		pref = Preferences.getInstance(context);
		
		connectionManager = new ConnectionManager(context);
	}
	
	public void setTitleBar(boolean home, boolean back, int title) {
		TextView titleText = (TextView) findViewById(R.id.title_text);
		ImageView titleHome = (ImageView) findViewById(R.id.title_home);
		titleBack = (ImageView) findViewById(R.id.title_back);
		
		titleText.setText(title);
		
		if (home) {
			titleHome.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, MenuActivity.class);
					startActivity(intent);
				}
			});
		}
		else {
			titleHome.setVisibility(View.GONE);
		}
		
		if (back) {
			titleBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
		}
		else {
			titleBack.setVisibility(View.GONE);
		}

	}//setTitleBar
	
	public void setTitle(boolean home, boolean back, String title) {
		TextView titleText = (TextView) findViewById(R.id.title_text);
		ImageView titleHome = (ImageView) findViewById(R.id.title_home);
		titleBack = (ImageView) findViewById(R.id.title_back);
		
		titleText.setText(title);
		
		if (home) {
			titleHome.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, MenuActivity.class);
					startActivity(intent);
				}
			});
		}
		else {
			titleHome.setVisibility(View.GONE);
		}
		
		if (back) {
			titleBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
				}
			});
		}
		else {
			titleBack.setVisibility(View.GONE);
		}

	}//setTitleBar
	
	public boolean showDialog(Context ctx, String msg) {
		// TODO Auto-generated method stub
		this.context = ctx;
		flag = true;
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg)
		       .setCancelable(false)
		       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                //do things
		        	   dialog.dismiss();
		        	   flag = true;
		           }
		       })
		       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                //do things
		        	   dialog.dismiss();
		        	   flag = false;
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
		
		return flag;
	}//showDialog
}
