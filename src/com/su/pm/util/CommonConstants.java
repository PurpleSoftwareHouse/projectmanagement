package com.su.pm.util;

public class CommonConstants {

	public static String ENGLISH_LOCALE = "en";
	public static String MYANMAR_LOCALE = "my";
	public static String DATABASE_NAME = "Application.db";
	
	public static String POST="post";
	public static String GET="get";
	public static String PUT="put";
	
	/***********URL*********/
	public static String HOST = "http://128.199.142.149:8282/";
	
	public static String BACKUP = "api/v1/backup";
	public static String RESTORE = "api/v1/restore";
}
