package com.su.pm.util;

/*
 * @author SuThwe
 * @date 3 March 2016
 * */

import android.app.Application;

import com.activeandroid.ActiveAndroid;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        
        //Notice this initialization code here
        ActiveAndroid.initialize(this);
    }
}
