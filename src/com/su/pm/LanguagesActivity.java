package com.su.pm;

/*
 * @author SuThwe
 * @date 12 May 2016
 * */

import java.util.Locale;

import com.su.pm.util.BaseActivity;
import com.su.pm.util.CommonConstants;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class LanguagesActivity extends BaseActivity {

	RadioGroup rdGroup;
	
	Locale myLocale;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_languages);

		rdGroup = (RadioGroup) findViewById(R.id.rd_languages_group);
		
		setTitleBar(true, true, R.string.change_languages);
		
		rdGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
					case R.id.myanmar:
						pref.setLOCALE(CommonConstants.MYANMAR_LOCALE);
						setLocale(CommonConstants.MYANMAR_LOCALE);
						break;
	
					case R.id.english:
						pref.setLOCALE(CommonConstants.ENGLISH_LOCALE);
						setLocale(CommonConstants.ENGLISH_LOCALE);
						break;
				}
			}
		});
		
	}//onCreate

	public void setLocale(String lang) {
		 
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MenuActivity.class);
        startActivity(refresh);
    }//setLocale
}
