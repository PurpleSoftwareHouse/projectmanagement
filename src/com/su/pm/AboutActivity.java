package com.su.pm;

/*
 * @author SuThwe
 * @date 4 Jun 2016
 * */

import com.su.pm.util.BaseActivity;

import android.os.Bundle;

public class AboutActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		setTitleBar(true, true, R.string.about);
		
		
		
	}//onCreate

}
