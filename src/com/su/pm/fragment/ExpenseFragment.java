package com.su.pm.fragment;

/*
 * @author SuThwe
 * @date 26 February 2016
 * */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.su.pm.models.Account;
import com.su.pm.models.Expense;
import com.su.pm.R;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ExpenseFragment extends Fragment implements ValidationListener, TextWatcher {
	
	@NotEmpty
	AutoCompleteTextView editProjectName;
	@NotEmpty
	EditText editDate;
	@NotEmpty
	EditText editExpenseAmt;
	@NotEmpty
	AutoCompleteTextView editTitle;
	@NotEmpty
	AutoCompleteTextView editSubTitle;
	@NotEmpty
	EditText editPayment;
	@NotEmpty
	EditText editPaid;
	@NotEmpty
	EditText editCredit;
	EditText editReason;
	Button btnAdd;
	
	Date date;
	String calDate;
	ListView listView;
	ArrayAdapter<String> adapter;
	String listValue;
	EditText editList;
	List<String> listProName, listTitle, listSubTitle, listPayment; 
	ArrayAdapter<String> proNamesAdapter, titleAdapter, subTitleAdapter;
	
	//datePicker
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener datePicker;
	
	//Validation
	Validator validator;
	
	//stop typing
	long idle_min = 1000; // 4 seconds after user stops typing
	long last_text_edit = 0;
	Handler h = new Handler();
	boolean already_queried = false;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.expense_frg, container, false);
        
        editProjectName = (AutoCompleteTextView) rootView.findViewById(R.id.project_name);
        editDate = (EditText) rootView.findViewById(R.id.income_date);
    	editExpenseAmt = (EditText) rootView.findViewById(R.id.amount);
    	editTitle = (AutoCompleteTextView) rootView.findViewById(R.id.title);
    	editSubTitle = (AutoCompleteTextView) rootView.findViewById(R.id.sub_title);
    	editReason = (EditText) rootView.findViewById(R.id.reason);
    	editPayment = (EditText) rootView.findViewById(R.id.payment);
    	editPaid = (EditText) rootView.findViewById(R.id.paid);
    	editCredit = (EditText) rootView.findViewById(R.id.credit);
    	btnAdd = (Button) rootView.findViewById(R.id.btn_expense);
    	
		//Validation
		validator = new Validator(this);
		validator.setValidationListener((ValidationListener) this);
		
		editDate.setText(getCurrentDate());

    	editExpenseAmt.addTextChangedListener(this);
    	editPaid.addTextChangedListener(this);
    	
		//add to autocomplete project names
		listProName = Expense.getProNames();
		proNamesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listProName);
		editProjectName.setAdapter(proNamesAdapter);
		editProjectName.setThreshold(1);
		
		//add to autocomplete titles
		listTitle = Expense.getTitles();
		titleAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listTitle);
		editTitle.setAdapter(titleAdapter);
		editTitle.setThreshold(1);
		
		//add to autocomplete titles
		listSubTitle = Expense.getSubTitles();
		subTitleAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listSubTitle);
		editSubTitle.setAdapter(subTitleAdapter);
		editSubTitle.setThreshold(1);
		
    	editPayment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PaymentDialog();
			}
		});
    	
    	editDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
			}
		});
    	
    	//Date Listener
		datePicker = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				
				date = myCalendar.getTime();
				calDate = java.text.DateFormat.getDateInstance().format(date);

				editDate.setText(calDate);
			}
			
		};
		
		btnAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validator.validate();
			}
		});
        
        return rootView;
    }//onCreateView
	
	@SuppressLint("SimpleDateFormat")
	private String getCurrentDate() {
		// TODO Auto-generated method stub
		myCalendar.get(Calendar.YEAR); 
		myCalendar.get(Calendar.MONTH); 
		myCalendar.get(Calendar.DAY_OF_MONTH);

		date = myCalendar.getTime();
		calDate = java.text.DateFormat.getDateInstance().format(date);
		
		return calDate;
	}

	private void PaymentDialog() {
		// TODO Auto-generated method stub
		listPayment = new ArrayList<String>();
		listPayment.addAll(Account.getAccountName());
		
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.list_dialog_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		listView = (ListView) dialog.findViewById(R.id.lst_dialog);
		adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listPayment);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				editPayment.setText(listPayment.get(pos));
			}
		});
		
		dialog.show();
	}//PaymentDialog
	
	private void Save() {
		// TODO Auto-generated method stub
		Expense expense = new Expense();
		expense.project_name = editProjectName.getText().toString();
		expense.expense_date = date;
		expense.amount = editExpenseAmt.getText().toString();
		expense.title = editTitle.getText().toString();
		expense.sub_title = editSubTitle.getText().toString();
		expense.payment = editPayment.getText().toString();
		expense.paid = editPaid.getText().toString();
		expense.credit = editCredit.getText().toString();
		expense.reason = editReason.getText().toString();
		expense.save();		

		List<Account> accounts = Account.getAllAccounts();
		for (Account acc : accounts) {
			if (acc.acc_name.equals(expense.payment)) {
				acc.opening_balance -= Double.valueOf(expense.paid);
				acc.save();
				break;
			}
		}
		
		editProjectName.setText("");
		editDate.setText("");
		editExpenseAmt.setText("");
		editTitle.setText("");
		editSubTitle.setText("");
		editPayment.setText("");
		editPaid.setText("");
		editCredit.setText("");
		editReason.setText("");
		
		Toast.makeText(getActivity(), R.string.data_saved, Toast.LENGTH_LONG).show();
	}//Save
	
	/******** calculate credit amount **********/
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		// dispatch after done typing (1 sec after)
		last_text_edit = System.currentTimeMillis();
		h.postDelayed(input_finish_checker, idle_min); 
	}
	
	private Runnable input_finish_checker = new Runnable() {
	    public void run() {
            if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                 // user hasn't changed the EditText for longer than
                 // the min delay (with half second buffer window)
                 /*if (!already_queried) { // don't do this stuff twice.
                     already_queried = true;*/
                     //do stuff
                     if (editExpenseAmt.getText().toString().length() > 0 && editPaid.getText().toString().length() > 0) {
     					int amount = Integer.parseInt(editExpenseAmt.getText().toString());
     					int paid = Integer.parseInt(editPaid.getText().toString());
     					
     					if (amount < paid) {
							editPaid.setError(getResources().getString(R.string.paid_amount_cant_be_greater_than)
											  +" "+ getResources().getString(R.string.expense_amount));
						}
     					else {
     						editCredit.setText("" + (amount - paid));
     					}
     				}
                 /*}*/
            }
	    }//run
	};

	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		Save();
	}//onValidationSucceeded

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		// TODO Auto-generated method stub
		for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
	}//onValidationFailed
}
