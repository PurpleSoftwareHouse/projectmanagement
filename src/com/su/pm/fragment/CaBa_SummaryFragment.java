package com.su.pm.fragment;

/*
 * @author SuThwe
 * @date 16 March 2016
 * */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.su.pm.InEx_TransactionActivity;
import com.su.pm.R;
import com.su.pm.models.Expense;
import com.su.pm.models.Income;
import com.su.pm.models.Lend;
import com.su.pm.models.Project;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class CaBa_SummaryFragment extends Fragment implements OnItemSelectedListener {
	
	TextView txtCashInHand;
	TextView txtCashAtBank;
	TextView txtLoan;
	Spinner spMonth;
	Spinner spYear;
	LinearLayout horizonLayout;
	
	String[] month_arr = {"Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	String[] year_arr;
	static List<Project> projects, projects1;
	List<Project> filterList;
	List<Lend> lends;
	double cash_in_hand, cash_at_bank, loan;
	String year, month, fyear, fmonth;
	SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
	SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM");
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.ca_ba_summary_frg, container, false);
        txtCashInHand = (TextView) rootView.findViewById(R.id.cash_in_hand);
        txtCashAtBank = (TextView) rootView.findViewById(R.id.cash_at_bank);
        txtLoan = (TextView) rootView.findViewById(R.id.loan_amount);
        spMonth = (Spinner) rootView.findViewById(R.id.month);
        spYear = (Spinner) rootView.findViewById(R.id.year);
        horizonLayout = (LinearLayout) rootView.findViewById(R.id.horizon_layout);
        
        //calculate Cash In Hand
        calculateCashInHand();
        
        //calculate Cash At Bank
        calculateCashAtBank();
        
        //calculate Loan
        calculateLoan();

        //set month to spinner
        ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, month_arr);
        spMonth.setAdapter(monthAdapter);
        
        //set year to spinner
        year_arr = new String[102];
        int year;
        year_arr[0] = "Year";
        for (int i = 1; i <= 101; i++) {
        	year = 1979 + i;
			year_arr[i] = ""+ year;
		}
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, year_arr);
        spYear.setAdapter(yearAdapter);

        getProjects();
        
        spMonth.setOnItemSelectedListener(this);
        spYear.setOnItemSelectedListener(this);
        
        return rootView;
    }//onCreateView
	
	private void calculateCashInHand() {
		// TODO Auto-generated method stub
		projects = Income.cashIncome();
		cash_in_hand = 0.0;
		for(Project pro : projects) {
			if (pro.paid != null) {
				cash_in_hand += Double.valueOf(pro.paid);
			}
		}
		
		txtCashInHand.setText("" + cash_in_hand);
	}//calculateCashInHand

	private void calculateCashAtBank() {
		// TODO Auto-generated method stub
		projects.addAll(Income.bankIncome());
		projects1 = Income.bankIncome();
		cash_at_bank = 0.0;
		for(Project pro : projects1) {
			if (pro.paid != null) {
				cash_at_bank += Double.valueOf(pro.paid);
			}
		}
		
		txtCashAtBank.setText("" + cash_at_bank);
	}//calculateCashAtBank
	
	private void calculateLoan() {
		// TODO Auto-generated method stub
		lends = Lend.cashLoan();
		lends.addAll(Lend.bankLoan());
		loan = 0.0;
		for(Lend le : lends) {
			if (le.paid != null) {
				loan += Double.valueOf(le.paid);
			}
		}
		
		txtLoan.setText("" + loan);
	}//calculateLoan

	private void getProjects() {
		// TODO Auto-generated method stub
		projects.addAll(Expense.cashExpense());
//		projects.addAll(Lend.cashLend());
//		projects.addAll(Borrow.cashBorrow());
		
		projects.addAll(Expense.bankExpense());
//		projects.addAll(Lend.bankLend());
//		projects.addAll(Borrow.bankBorrow());
		
//		projects.addAll(Transaction.getTransactionAsProjects());
		
		Collections.sort(projects, new Comparator<Project>() {
		  public int compare(Project p1, Project p2) {
		      return p2.project_date.compareTo(p1.project_date);
		  }
		});
		
		addHorizontalRow(projects);
	}

	private void addHorizontalRow(final List<Project> proList) {
		// TODO Auto-generated method stub
		LayoutInflater inflator = getActivity().getLayoutInflater();
		ViewGroup headerLayout = (ViewGroup) inflator.inflate(R.layout.cash_horizontal_header, horizonLayout, false);
		horizonLayout.addView(headerLayout);
		TextView txtDate = null, txtName = null, txtTitle = null, txtIn = null, txtOut = null;
		Project pro;
		String dd;
		
		for (int i = 0; i < proList.size(); i++) {
			ViewGroup rowLayout = (ViewGroup) inflator.inflate(R.layout.cash_horizontal_listview, horizonLayout, false);
			txtDate = (TextView) rowLayout.findViewById(R.id.txt_date);
			txtTitle = (TextView) rowLayout.findViewById(R.id.title);
			txtName = (TextView) rowLayout.findViewById(R.id.name);
			txtIn = (TextView) rowLayout.findViewById(R.id.in);
			txtOut = (TextView) rowLayout.findViewById(R.id.out);
			
			rowLayout.setTag(""+ i);
			txtDate.setTag(""+ i);
			pro = proList.get(i);
			dd = java.text.DateFormat.getDateInstance().format(pro.project_date);
			txtDate.setText(dd);
			txtTitle.setText(pro.type);
			txtName.setText(pro.project_name);
			
			if (pro.type.equals("Income")) {
				txtIn.setText(pro.paid);
				txtOut.setText("-");
			}
			else if (pro.type.equals("Expense")) {
				txtIn.setText("-");
				txtOut.setText(pro.credit);
			}
			
			rowLayout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Project project = proList.get(Integer.parseInt(""+ v.getTag()));
					Intent intent = new Intent(getActivity(), InEx_TransactionActivity.class);
					intent.putExtra("Project", project);
					startActivity(intent);
				}
			});
			
			horizonLayout.addView(rowLayout);
		}

	}//addHorizontalRow

	private void filter() {
		// TODO Auto-generated method stub		
		filterList = new ArrayList<Project>();
		
		if (year.equals("Year") || month.equals("Month")) {
			for(Project pro : projects){
				fyear = yearFormat.format(pro.project_date);
				fmonth = monthFormat.format(pro.project_date);
				
				if (year.equals(fyear) || month.equals(fmonth)) {
					filterList.add(pro);
				}
			}
		}
		else {
			for(Project pro : projects){
				fyear = yearFormat.format(pro.project_date);
				fmonth = monthFormat.format(pro.project_date);
				
				if (year.equals(fyear) && month.equals(fmonth)) {
					filterList.add(pro);
				}
			}
		}
		
		horizonLayout.removeAllViews();
		addHorizontalRow(filterList);
		
	}//filter

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		year = spYear.getSelectedItem().toString();
		month = spMonth.getSelectedItem().toString();
		if (!year.equals("Year") || !month.equals("Month")) {
			filter();
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

}
