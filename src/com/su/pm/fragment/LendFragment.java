package com.su.pm.fragment;

/*
 * @author SuThwe
 * @date 4 March 2016
 * */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.su.pm.R;
import com.su.pm.helper.AlarmReceiver;
import com.su.pm.models.Account;
import com.su.pm.models.Lend;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class LendFragment extends Fragment implements ValidationListener, TextWatcher {
	
	@NotEmpty
	EditText editDate;
	@NotEmpty
	EditText editAmount;
	@NotEmpty
	EditText editRate;
	@NotEmpty
	EditText editPer;
	@NotEmpty
	AutoCompleteTextView editName;
	@NotEmpty
	EditText editPhone;
	@NotEmpty
	EditText editPayment;
	@NotEmpty
	EditText editPaid;
	@NotEmpty
	EditText editCredit;
	EditText editReason;
	@NotEmpty
	EditText editReminder;
	Button btnSave;

	Date date;
	String calDate;
	ListView listView;
	List<String> listNames, listPayment;
	Map<String, String> mapPhones;
	ArrayAdapter<String> adapter, nameAdapter;
	boolean reminder = false;
	
	//datePicker
	Calendar myCalendar = Calendar.getInstance(), cal_reminder;
	DatePickerDialog.OnDateSetListener datePicker;
	
	//timePicker
	TimePickerDialog.OnTimeSetListener timePicker;
	
	//Validation
	Validator validator;
	
	//stop typing
	long idle_min = 1000; // 4 seconds after user stops typing
	long last_text_edit = 0;
	Handler h = new Handler();
	boolean already_queried = false;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lend_frg, container, false);
        
        editDate = (EditText) rootView.findViewById(R.id.lend_date);
        editAmount = (EditText) rootView.findViewById(R.id.amount);
    	editRate = (EditText) rootView.findViewById(R.id.rate);
    	editPer = (EditText) rootView.findViewById(R.id.month_year);
    	editName = (AutoCompleteTextView) rootView.findViewById(R.id.name); 
    	editPhone = (EditText) rootView.findViewById(R.id.phone);
    	editPayment = (EditText) rootView.findViewById(R.id.payment);
    	editPaid = (EditText) rootView.findViewById(R.id.paid);
    	editCredit = (EditText) rootView.findViewById(R.id.credit);
    	editReason = (EditText) rootView.findViewById(R.id.reason);
    	btnSave = (Button) rootView.findViewById(R.id.btn_save);
    	editReminder = (EditText) rootView.findViewById(R.id.reminder);
    	
		//Validation
		validator = new Validator(this);
		validator.setValidationListener((ValidationListener) this);
		
		editDate.setText(getCurrentDate());

		editAmount.addTextChangedListener(this);
    	editPaid.addTextChangedListener(this);
    	
    	//add to autocomplete names
    	listNames = Lend.getNames();
    	mapPhones = Lend.getPhones();
    	nameAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listNames);
    	editName.setAdapter(nameAdapter);
    	editName.setThreshold(1);
    	
    	editName.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				editPhone.setText(mapPhones.get(listNames.get(position)));
			}
		});
    	
    	editPer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SelectMonthOrYear();
			}
		});
    	
    	editPayment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PaymentDialog();
			}
		});
    	
    	editDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
			}
		});
    	
    	editReminder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reminder = true;
				
				DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
			}
		});
    	
    	//Date Listener
		datePicker = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				
				if (reminder) {
					reminder = false;
					cal_reminder = myCalendar;
					TimePickerDialog timeDialog = new TimePickerDialog(getActivity(), 
							timePicker, 
							myCalendar.get(Calendar.HOUR_OF_DAY), 
							myCalendar.get(Calendar.MINUTE), 
							true);
					timeDialog.show();
				}
				else {
					date = myCalendar.getTime();
					calDate = java.text.DateFormat.getDateInstance().format(date);
					editDate.setText(calDate);
				}
			}
		};
		
		timePicker = new TimePickerDialog.OnTimeSetListener() {
			
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
				myCalendar.set(Calendar.MINUTE, minute);
				myCalendar.set(Calendar.SECOND, 0);
				
				editReminder.setText(java.text.DateFormat.getDateTimeInstance().format(myCalendar.getTime()));
			}
		};
        
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validator.validate();
			}
		});
		
        return rootView;
    }//onCreateView
	
	@SuppressLint("SimpleDateFormat")
	private String getCurrentDate() {
		// TODO Auto-generated method stub
		myCalendar.get(Calendar.YEAR); 
		myCalendar.get(Calendar.MONTH); 
		myCalendar.get(Calendar.DAY_OF_MONTH);

		date = myCalendar.getTime();
		calDate = java.text.DateFormat.getDateInstance().format(date);
		
		return calDate;
	}

	private void PaymentDialog() {
		// TODO Auto-generated method stub
		listPayment = new ArrayList<String>();
		listPayment.addAll(Account.getAccountName());
		
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.list_dialog_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		listView = (ListView) dialog.findViewById(R.id.lst_dialog);
		adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listPayment);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				editPayment.setText(listPayment.get(pos));
			}
		});
		
		dialog.show();
		
	}//PaymentDialog
	
	private void SelectMonthOrYear() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.month_or_year_dialog);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		final TextView txtMonth = (TextView) dialog.findViewById(R.id.per_month);
		final TextView txtYear = (TextView) dialog.findViewById(R.id.per_year);
		
		txtMonth.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editPer.setText(txtMonth.getText().toString());
				dialog.dismiss();
			}
		});
		
		txtYear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editPer.setText(txtYear.getText().toString());
				dialog.dismiss();
			}
		});
		
		dialog.show();
		
	}//SelectMonthOrYear
	
	private void Save() {
		// TODO Auto-generated method stub
		Lend lend = new Lend();
		lend.lend_date = date;
		lend.amount = editAmount.getText().toString();
		lend.rate = editRate.getText().toString();
		lend.month_or_year = editPer.getText().toString();
		lend.name = editName.getText().toString();
		lend.phone = editPhone.getText().toString();
		lend.payment = editPayment.getText().toString();
		lend.paid = editPaid.getText().toString();
		lend.credit = editCredit.getText().toString();
		lend.reason = editReason.getText().toString();
		lend.save();

		List<Account> accounts = Account.getAllAccounts();
		for (Account acc : accounts) {
			if (acc.acc_name.equals(lend.payment)) {
				acc.opening_balance += Double.valueOf(lend.paid);
				acc.save();
				break;
			}
		}
		
		//add reminder
		if (cal_reminder != null) {
			setReminder();
		}
		
		editDate.setText("");
		editAmount.setText("");
		editRate.setText("");
		editPer.setText("");
		editName.setText("");
		editPhone.setText("");
		editPayment.setText("");
		editPaid.setText("");
		editCredit.setText("");
		editReason.setText("");
		editReminder.setText("");
		
		Toast.makeText(getActivity(), R.string.data_saved, Toast.LENGTH_LONG).show();
	}//Save
	
	private void setReminder() {
		/* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(getActivity(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager manager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

		//removed 'interval' because just want to remind only once
        manager.set(AlarmManager.RTC_WAKEUP, cal_reminder.getTimeInMillis(), pendingIntent);
	}//setReminder
	
	/******** calculate credit amount **********/
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		// dispatch after done typing (1 sec after)
		last_text_edit = System.currentTimeMillis();
		h.postDelayed(input_finish_checker, idle_min); 
	}
	
	private Runnable input_finish_checker = new Runnable() {
	    public void run() {
            if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                 // user hasn't changed the EditText for longer than
                 // the min delay (with half second buffer window)
                 //do stuff
                 if (editAmount.getText().toString().length() > 0 && editPaid.getText().toString().length() > 0) {
 					int amount = Integer.parseInt(editAmount.getText().toString());
 					int paid = Integer.parseInt(editPaid.getText().toString());
 					
 					if (amount < paid) {
						editPaid.setError(getResources().getString(R.string.paid_amount_cant_be_greater_than)
										  +" "+ getResources().getString(R.string.income_amount));
					}
 					else {
 						editCredit.setText("" + (amount - paid));
 					}
 				}
            }
	    }//run
	};
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		Calendar current = Calendar.getInstance();
		if (cal_reminder.compareTo(current) > 0) {
			Save();
		}
		else {
			editReminder.setError(getResources().getString(R.string.reminder_date_error));
			Toast.makeText(getActivity(), getResources().getString(R.string.reminder_date_error), Toast.LENGTH_LONG).show();
		}
		
	}//onValidationSucceeded

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		// TODO Auto-generated method stub
		for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
	}//onValidationFailed
}
