package com.su.pm.fragment;

/*
 * @author SuThwe
 * @date 4 March 2016
 * */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.su.pm.LeBo_ProjectDetailActivity;
import com.su.pm.R;
import com.su.pm.models.Borrow;
import com.su.pm.models.Lend;
import com.su.pm.util.Preferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class LeBo_SummaryFragment extends Fragment {
	
	TextView txtTotal;
	ListView listSummary;

	CustomList customList;
	ArrayList<String> nameList;
	List<String> names, balanceList, lendList, borrowList;
	List<Lend> lends;
	List<Borrow> borrows;
	Preferences pref;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.le_bo_summary_frg, container, false);

        txtTotal = (TextView) rootView.findViewById(R.id.total_balance);
        listSummary = (ListView) rootView.findViewById(R.id.summary_list);

        names = new ArrayList<String>();
        balanceList = new ArrayList<String>();
        lendList = new ArrayList<String>();
        borrowList = new ArrayList<String>();
        pref = Preferences.getInstance(getActivity());
        
//        onLoadData();
        
        listSummary.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				pref.setPROJECT_NAME(nameList.get(position));
				pref.setBALANCE(balanceList.get(position));
				pref.setLEND(lendList.get(position));
				pref.setBORROW(borrowList.get(position));
				
				Intent intent = new Intent(getActivity(), LeBo_ProjectDetailActivity.class);
				startActivity(intent);
			}
		});
        
        return rootView;
    }//onCreateView
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			onLoadData();
		}
	}
	
	private void onLoadData() {
		// TODO Auto-generated method stub
		lends = Lend.getLends();
        for(Lend le : lends) {
        	names.add(le.name);
		}
        
        borrows = Borrow.getBorrows();
        for(Borrow br : borrows) {
        	names.add(br.name);
        }
        
        nameList = new ArrayList<String>(new HashSet<String>(names));
        
        //calculate amount of Lends for each projects
        double lend_amt;
        for (int i = 0; i < nameList.size(); i++) {
        	lend_amt = 0;
        	for(Lend le : lends) {
				if (le.name.equals(nameList.get(i))) {
					lend_amt += Double.valueOf(le.paid);
				}
			}
        	lendList.add(String.valueOf(lend_amt));
		}
        
        //calculate amount of Lends for each projects
        double borrow_amt;
        for (int i = 0; i < nameList.size(); i++) {
        	borrow_amt = 0;
        	for(Borrow br : borrows) {
				if (br.name.equals(nameList.get(i))) {
					borrow_amt += Double.valueOf(br.paid);
				}
			}
        	borrowList.add(String.valueOf(borrow_amt));
		}
        
        //calculate balance for each projects
        for (int i = 0; i < nameList.size(); i++) {
        	balanceList.add(String.valueOf( (Double.valueOf(lendList.get(i)) - (Double.valueOf(borrowList.get(i)) ))));
        }
        
        //calculate total balance
        Double balance = 0.0;
        for(String bal : balanceList) {
        	balance += Double.valueOf(bal);
        }

        txtTotal.setText(getResources().getString(R.string.total_balance_mmk)+ " " + String.valueOf(balance));

        customList = new CustomList(getActivity(), R.layout.in_ex_summary_listview, nameList);
        listSummary.setAdapter(customList);
	}//onLoadData
	
	private class CustomList extends ArrayAdapter<String> {
		List<String> nameList;
		Context context;

		public CustomList(Context c, int resource, List<String> nameList) {
			super(c, resource, nameList);
			this.nameList = nameList;
			this.context = c;
		}
		
		@Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflator.inflate(R.layout.in_ex_summary_listview, parent, false);
            
            TextView txtProjectName = (TextView) row.findViewById(R.id.project_name);
            TextView txtBalance = (TextView) row.findViewById(R.id.balance);
            TextView txtIncome = (TextView) row.findViewById(R.id.income);
            TextView txtExpense = (TextView) row.findViewById(R.id.expense);
            
            txtProjectName.setText(nameList.get(pos));
            txtBalance.setText(getResources().getString(R.string.balance)+ "  " + balanceList.get(pos));
            txtIncome.setText(getResources().getString(R.string.lend)+ "   " + lendList.get(pos));
            txtExpense.setText(getResources().getString(R.string.borrow)+ "  " + borrowList.get(pos));
            
            return(row);
        }
	}//CustomList
}
