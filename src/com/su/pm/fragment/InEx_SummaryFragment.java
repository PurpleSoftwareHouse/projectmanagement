package com.su.pm.fragment;

/*
 * @author SuThwe
 * @date 26 February 2016
 * */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.su.pm.models.Expense;
import com.su.pm.models.Income;
import com.su.pm.util.Preferences;
import com.su.pm.InEx_ProjectDetailActivity;
import com.su.pm.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class InEx_SummaryFragment extends Fragment {
	
	TextView txtTotal;
	ListView listSummary;
	
	CustomList customList;
	ArrayList<String> projectNameList;
	List<String> projectNames, balanceList, incomeList, expenseList;
	List<Income> incomes;
	List<Expense> expenses;
	Preferences pref;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.in_ex_summary_frg, container, false);
        
        txtTotal = (TextView) rootView.findViewById(R.id.total_balance);
        listSummary = (ListView) rootView.findViewById(R.id.summary_list);
        
        projectNames = new ArrayList<String>();
        balanceList = new ArrayList<String>();
        incomeList = new ArrayList<String>();
        expenseList = new ArrayList<String>();
        pref = Preferences.getInstance(getActivity());
        
//        onLoadData();
        
        listSummary.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				pref.setPROJECT_NAME(projectNameList.get(position));
				pref.setBALANCE(balanceList.get(position));
				pref.setINCOME(incomeList.get(position));
				pref.setEXPENSE(expenseList.get(position));
				
				Intent intent = new Intent(getActivity(), InEx_ProjectDetailActivity.class);
				startActivity(intent);
			}
		});

        return rootView;
    }//onCreateView
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			onLoadData();
		}
	}//setUserVisibleHint
	
	private void onLoadData() {
		// TODO Auto-generated method stub
		incomes = Income.getIncomes();
        for(Income in : incomes) {
        	projectNames.add(in.project_name);
		}
        
        expenses = Expense.getExpenses();
        for(Expense ex : expenses) {
        	projectNames.add(ex.project_name);
        }
        
        projectNameList = new ArrayList<String>(new HashSet<String>(projectNames));
        
        //calculate amount of Incomes for each projects
        double income_amt;
        for (int i = 0; i < projectNameList.size(); i++) {
        	income_amt = 0;
        	for(Income in : incomes) {
				if (in.project_name.equals(projectNameList.get(i))) {
					income_amt += Double.valueOf(in.paid);
				}
			}
        	incomeList.add(String.valueOf(income_amt));
		}
        
        //calculate amount of Incomes for each projects
        double expense_amt;
        for (int i = 0; i < projectNameList.size(); i++) {
        	expense_amt = 0;
        	for(Expense ex : expenses) {
				if (ex.project_name.equals(projectNameList.get(i))) {
					expense_amt += Double.valueOf(ex.paid);
				}
			}
        	expenseList.add(String.valueOf(expense_amt));
		}
        
        //calculate balance for each projects
        for (int i = 0; i < projectNameList.size(); i++) {
        	balanceList.add(String.valueOf( (Double.valueOf(incomeList.get(i)) - (Double.valueOf(expenseList.get(i)) ))));
        }
        
        //calculate total balance
        Double balance = 0.0;
        for(String bal : balanceList) {
        	balance += Double.valueOf(bal);
        }

        txtTotal.setText(getResources().getString(R.string.total_balance_mmk)+ " " + String.valueOf(balance));

        customList = new CustomList(getActivity(), R.layout.in_ex_summary_listview, projectNameList);
        listSummary.setAdapter(customList);
	}//onLoadData
	
	private class CustomList extends ArrayAdapter<String> {
		List<String> projectNameList;
		Context context;

		public CustomList(Context c, int resource, List<String> projectNameList) {
			super(c, resource, projectNameList);
			this.projectNameList = projectNameList;
			this.context = c;
		}
		
		@Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflator.inflate(R.layout.in_ex_summary_listview, parent, false);
            
            TextView txtProjectName = (TextView) row.findViewById(R.id.project_name);
            TextView txtBalance = (TextView) row.findViewById(R.id.balance);
            TextView txtIncome = (TextView) row.findViewById(R.id.income);
            TextView txtExpense = (TextView) row.findViewById(R.id.expense);
            
            txtProjectName.setText(projectNameList.get(pos));
            txtBalance.setText(getResources().getString(R.string.balance)+ "  " + balanceList.get(pos));
            txtIncome.setText(getResources().getString(R.string.income)+ "   " + incomeList.get(pos));
            txtExpense.setText(getResources().getString(R.string.expense)+ "  " + expenseList.get(pos));
            
            return(row);
        }
	}//CustomList
}
