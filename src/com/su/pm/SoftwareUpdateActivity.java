package com.su.pm;

import com.su.pm.util.BaseActivity;

import android.os.Bundle;


public class SoftwareUpdateActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_software_update);

		setTitleBar(true, true, R.string.software_update);
	}//onCreate

}
