package com.su.pm;

/*
 * @author SuThwe
 * @date 17 March 2016
 * */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.su.pm.models.Borrow;
import com.su.pm.models.Expense;
import com.su.pm.models.Income;
import com.su.pm.models.Lend;
import com.su.pm.models.Project;
import com.su.pm.util.BaseActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TransactionEditActivity extends BaseActivity implements TextWatcher {
	Context context = this;

	LinearLayout projectLayout;
	LinearLayout nameLayout;
	@NotEmpty
	EditText valueDate, valueProjectName, valueTitle, valueSubTitle, valueName, valuePhone, valuePayment, valueAmount, valuePaid, valueCredit, valueReason;
	Button btnSave;
	
	Project project;
	String type;
	
	//datePicker
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener datePicker;

	//stop typing
	long idle_min = 1000; // 4 seconds after user stops typing
	long last_text_edit = 0;
	Handler h = new Handler();
	boolean already_queried = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);
		
		valueDate = (EditText) findViewById(R.id.txt_date_value);
		valueProjectName = (EditText) findViewById(R.id.project_name_value);
		valueTitle = (EditText) findViewById(R.id.title_value);
		valueSubTitle = (EditText) findViewById(R.id.sub_title_value);
		valueName = (EditText) findViewById(R.id.name_value);
		valuePhone = (EditText) findViewById(R.id.phone_value);
		valuePayment = (EditText) findViewById(R.id.payment_value);
		valueAmount = (EditText) findViewById(R.id.amount_value);
		valuePaid = (EditText) findViewById(R.id.paid_value);
		valueCredit = (EditText) findViewById(R.id.credit_value);
		valueReason = (EditText) findViewById(R.id.reason_value);
		projectLayout = (LinearLayout) findViewById(R.id.project_layout);
		nameLayout = (LinearLayout) findViewById(R.id.name_layout);
		btnSave = (Button) findViewById(R.id.save);
		
		setTitleBar(true, true, R.string.in_ex_action_bar_title);  
		
		project = (Project) getIntent().getSerializableExtra("Project");
		type = project.type;
		
		if (type.equals("Income") || type.equals("Expense")) {
			nameLayout.setVisibility(View.GONE);
			setTitleBar(true, true, R.string.income_expense); 
		}
		else if (type.equals("Lend") || type.equals("Borrow")) {
			projectLayout.setVisibility(View.GONE);
			setTitleBar(true, true, R.string.lend_borrow); 
		}
		
		String txtDate = java.text.DateFormat.getDateInstance().format(project.project_date);
		valueDate.setText(txtDate);
		valueProjectName.setText(project.project_name);
		valueTitle.setText(project.title);
		valueSubTitle.setText(project.sub_title);
		valueName.setText(project.project_name);
		valuePhone.setText(project.phone);
		valuePayment.setText(project.payment);
		valueAmount.setText(project.amount);
		valuePaid.setText(project.paid);
		valueCredit.setText(project.credit);
		valueReason.setText(project.reason);

		valueAmount.addTextChangedListener(this);
		valuePaid.addTextChangedListener(this);
		
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editProject();
			}
		});
		
		valuePayment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PaymentDialog();
			}
		});
		
		valueDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatePickerDialog dateDialog = new DatePickerDialog(TransactionEditActivity.this,
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
			}
		});
    	
    	//Date Listener
		datePicker = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

				String calDate = java.text.DateFormat.getDateInstance().format(myCalendar.getTime());
				valueDate.setText(calDate);
			}
			
		};
		
	}//onCreate
	
	private void PaymentDialog() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(TransactionEditActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.payment_dialog);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		final TextView txtCash = (TextView) dialog.findViewById(R.id.payment_cash);
		final TextView txtBank = (TextView) dialog.findViewById(R.id.payment_bank);
		
		txtCash.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				valuePayment.setText(txtCash.getText().toString());
				dialog.dismiss();
			}
		});
		
		txtBank.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				valuePayment.setText(txtBank.getText().toString());
				dialog.dismiss();
			}
		});
		
		dialog.show();
		
	}//PaymentDialog

	private void editProject() {
		// TODO Auto-generated method stub
		Project pro = new Project();
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
		Date dd = null;
		try {
			dd= format.parse(valueDate.getText().toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (type.equals("Income")) {
			Income in = Income.load(Income.class, project.id);
			in.project_name = valueProjectName.getText().toString();
			in.title = valueTitle.getText().toString();
			in.sub_title = valueSubTitle.getText().toString();
			in.payment = valuePayment.getText().toString();
			in.amount = valueAmount.getText().toString();
			in.paid = valuePaid.getText().toString();
			in.credit = valueCredit.getText().toString();
			in.reason = valueReason.getText().toString();
			in.income_date = dd;
			in.save();

			pro = Income.convertProject(in);
			Intent intent = new Intent(context, InEx_TransactionActivity.class);
			intent.putExtra("Project", pro);
			startActivity(intent);
		}
		else if (type.equals("Expense")) {
			Expense ex = Expense.load(Expense.class, project.id);
			ex.project_name = valueProjectName.getText().toString();
			ex.title = valueTitle.getText().toString();
			ex.sub_title = valueSubTitle.getText().toString();
			ex.payment = valuePayment.getText().toString();
			ex.amount = valueAmount.getText().toString();
			ex.paid = valuePaid.getText().toString();
			ex.credit = valueCredit.getText().toString();
			ex.reason = valueReason.getText().toString();
			ex.expense_date = dd;
			ex.save();

			pro = Expense.convertProject(ex);
			Intent intent = new Intent(context, InEx_TransactionActivity.class);
			intent.putExtra("Project", pro);
			startActivity(intent);
		}
		else if (type.equals("Lend")) {
			Lend le = Lend.load(Lend.class, project.id);
			le.name = valueName.getText().toString();
			le.phone = valuePhone.getText().toString();
			le.payment = valuePayment.getText().toString();
			le.amount = valueAmount.getText().toString();
			le.paid = valuePaid.getText().toString();
			le.credit = valueCredit.getText().toString();
			le.reason = valueReason.getText().toString();
			le.lend_date = dd;
			le.save();

			pro = Lend.convertProject(le);
			Intent intent = new Intent(context, LeBo_TransactionActivity.class);
			intent.putExtra("Project", pro);
			startActivity(intent);
		}
		else if (type.equals("Borrow")) {
			Borrow bo = Borrow.load(Borrow.class, project.id);
			bo.name = valueName.getText().toString();
			bo.phone = valuePhone.getText().toString();
			bo.payment = valuePayment.getText().toString();
			bo.amount = valueAmount.getText().toString();
			bo.paid = valuePaid.getText().toString();
			bo.credit = valueCredit.getText().toString();
			bo.reason = valueReason.getText().toString();
			bo.borrow_date = dd;
			bo.save();

			pro = Borrow.convertProject(bo);
			Intent intent = new Intent(context, LeBo_TransactionActivity.class);
			intent.putExtra("Project", pro);
			startActivity(intent);
		}
	}//editProject
	
	/******** calculate credit amount **********/
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		// dispatch after done typing (1 sec after)
		last_text_edit = System.currentTimeMillis();
		h.postDelayed(input_finish_checker, idle_min); 
	}
	
	private Runnable input_finish_checker = new Runnable() {
	    public void run() {
            if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                 // user hasn't changed the EditText for longer than
                 // the min delay (with half second buffer window)
                 //do stuff
                 if (valueAmount.getText().toString().length() > 0 && valuePaid.getText().toString().length() > 0) {
 					int amount = Integer.parseInt(valueAmount.getText().toString());
 					int paid = Integer.parseInt(valuePaid.getText().toString());
 					
 					if (amount < paid) {
						valuePaid.setError(getResources().getString(R.string.paid_amount_cant_be_greater_than)
										  +" "+ getResources().getString(R.string.income_amount));
					}
 					else {
 						valueCredit.setText("" + (amount - paid));
 					}
 				}
            }
	    }//run
	};
	
}
