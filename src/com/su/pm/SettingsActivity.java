package com.su.pm;

/*
 * @author SuThwe
 * @date 5 May 2016
 * */

import com.su.pm.util.BaseActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class SettingsActivity extends BaseActivity {
	Context context = this;
	
	TextView about;
	TextView change_pwd;
	TextView backup;
	TextView languages;
	TextView feedback;
	TextView contact_us;
	TextView update;
	TextView print;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		about = (TextView) findViewById(R.id.about);
		change_pwd = (TextView) findViewById(R.id.change_password);
		backup = (TextView) findViewById(R.id.backup);
		languages = (TextView) findViewById(R.id.languages);
		feedback = (TextView) findViewById(R.id.feedback);
		contact_us = (TextView) findViewById(R.id.contact);
		update = (TextView) findViewById(R.id.update);
		print = (TextView) findViewById(R.id.print);
		
		setTitleBar(true, true, R.string.setting);
		
		about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, AboutActivity.class);
				startActivity(intent);
			}
		});
		
		change_pwd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, ChangePasswordActivity.class);
				startActivity(intent);
			}
		});
		
		backup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, BackupActivity.class);
				startActivity(intent);
			}
		});
		
		languages.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, LanguagesActivity.class);
				startActivity(intent);
			}
		});
		
		feedback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, FeedbackActivity.class);
				startActivity(intent);
			}
		});
		
		contact_us.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, ContactUsActivity.class);
				startActivity(intent);
			}
		});
		
		update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, SoftwareUpdateActivity.class);
				startActivity(intent);
			}
		});

		print.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, PrintActivity.class);
				startActivity(intent);
			}
		});
		
	}//onCreate

}
