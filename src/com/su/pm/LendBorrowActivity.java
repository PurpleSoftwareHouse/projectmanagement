package com.su.pm;

/*
 * @author SuThwe
 * @date 4 March 2016
 * */

import com.su.pm.adapter.LeBo_PagerAdapter;
import com.su.pm.util.BaseFragmentActivity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.content.Context;
import android.content.Intent;

public class LendBorrowActivity extends BaseFragmentActivity {
	Context context = this;

	ViewPager viewPager;
	LeBo_PagerAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lend_borrow);
		
		viewPager = (ViewPager) findViewById(R.id.view_pager);
		
		//for viewpager
        mAdapter = new LeBo_PagerAdapter(getSupportFragmentManager()); 
        viewPager.setAdapter(mAdapter);
		
		setTitleBar(true, true, R.string.lend_borrow);

        String page = getIntent().getStringExtra("Summary");
        if (page != null) {
			viewPager.setCurrentItem(2);
		}
	}//onCreate
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(context, MenuActivity.class);
		startActivity(intent);
	}

}
