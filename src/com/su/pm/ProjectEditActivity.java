package com.su.pm;

/*
 * @author SuThwe
 * @date 17 March 2016
 * */

import java.util.Calendar;
import java.util.Date;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.su.pm.models.Income;
import com.su.pm.models.Project;
import com.su.pm.util.BaseActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class ProjectEditActivity extends BaseActivity {
	Context context = this;
	
	TextView txtTransTitle;
	TextView txtDate;
	TextView txtProjectName;
	TextView txtTitle;
	TextView txtSubTitle;
	TextView txtPayment;
	TextView txtAmount;
	TextView txtReason;
	@NotEmpty
	EditText valueDate, valueProjectName, valueTitle, valueSubTitle, valuePayment, valueAmount, valueReason;
	Button btnSave;
	
	Project project;
	String type;
	
	//datePicker
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener datePicker;
	Date date;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);
		
		valueDate = (EditText) findViewById(R.id.txt_date_value);
		valueProjectName = (EditText) findViewById(R.id.project_name_value);
		valueTitle = (EditText) findViewById(R.id.title_value);
		valueSubTitle = (EditText) findViewById(R.id.sub_title_value);
		valuePayment = (EditText) findViewById(R.id.payment_value);
		valueAmount = (EditText) findViewById(R.id.amount_value);
		valueReason = (EditText) findViewById(R.id.reason_value);
		btnSave = (Button) findViewById(R.id.save);
		
		setTitleBar(true, true, R.string.in_ex_action_bar_title);  
		
		project = (Project) getIntent().getSerializableExtra("Project");
		type = getIntent().getStringExtra("Type");
		
		String txtDate = java.text.DateFormat.getDateInstance().format(project.project_date);
		valueDate.setText(txtDate);
		valueProjectName.setText(project.project_name);
		valueTitle.setText(project.title);
		valueSubTitle.setText(project.sub_title);
		valuePayment.setText(project.payment);
		valueAmount.setText(project.amount);
		valueReason.setText(project.reason);
		
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editProject();
			}
		});
		
		valueDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatePickerDialog dateDialog = new DatePickerDialog(ProjectEditActivity.this,
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
			}
		});
    	
    	//Date Listener
		datePicker = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				
				date = myCalendar.getTime();
				String calDate = java.text.DateFormat.getDateInstance().format(date);

				valueDate.setText(calDate);
			}
			
		};
		
	}//onCreate
	
	/*@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(context, InEx_TransactionActivity.class);
		startActivity(intent);
	}*/

	private void editProject() {
		// TODO Auto-generated method stub
		Project pro = new Project();
		if (type.equals("Income")) {
			Income in = Income.load(Income.class, project.id);
//			in.income_date = valueDate.getText().toString();
			in.project_name = valueProjectName.getText().toString();
			in.title = valueTitle.getText().toString();
			in.sub_title = valueSubTitle.getText().toString();
			in.payment = valuePayment.getText().toString();
			in.amount = valueAmount.getText().toString();
			in.reason = valueReason.getText().toString();
			in.save();

			pro.id = in.getId();
			pro.project_name = in.project_name;
			pro.project_date = in.income_date;
			pro.amount = in.amount;
			pro.title = in.title;
			pro.sub_title = in.sub_title;
			pro.payment = in.payment;
			pro.paid = in.paid;
			pro.credit = in.credit;
			pro.reason = in.reason;
			pro.type = "Income";
			
//			Toast.makeText(context, "Saved", Toast.LENGTH_LONG).show();
		}
		
		Intent intent = new Intent(context, InEx_TransactionActivity.class);
		intent.putExtra("Project", pro);
		intent.putExtra("Type", type);
		startActivity(intent);
	}
}
