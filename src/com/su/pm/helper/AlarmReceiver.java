package com.su.pm.helper;

import com.su.pm.R;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

/*
 * @author SuThwe
 * @date 18 Jun 2016
 * */

public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		showNotification(context, intent);
	}

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showNotification(Context context, Intent intent) {
        //define sound URI
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification mNotification = new Notification.Builder(context)
                .setContentTitle("Project Management")
                .setContentText("Remainder")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .setAutoCancel(true)
//                .addAction(0, "Remind", pIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mNotification);

    }//showNotification

}
