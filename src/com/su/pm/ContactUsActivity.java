package com.su.pm;

import com.su.pm.util.BaseActivity;

import android.os.Bundle;


public class ContactUsActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us);

		setTitleBar(true, true, R.string.contact_us);
	}//onCreate

}
