package com.su.pm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.su.pm.api.JSONParser;
import com.su.pm.util.BaseActivity;
import com.su.pm.util.CommonConstants;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BackupActivity extends BaseActivity {

	EditText editBackup;
	Button btnBackup;
	Button btnRestore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_backup);

		editBackup = (EditText) findViewById(R.id.backup_email);
		btnBackup = (Button) findViewById(R.id.btn_backup);
		btnRestore = (Button) findViewById(R.id.btn_restore);
		
		setTitleBar(true, true, R.string.backup);
		
		btnBackup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Backup();
			}
		});
	}//onCreate

	private void Backup() {
		// TODO Auto-generated method stub
		File sd = Environment.getExternalStorageDirectory();
      	File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/data/"+ "com.su.pm" +"/databases/"+ CommonConstants.DATABASE_NAME;
        String backupDBPath = CommonConstants.DATABASE_NAME;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
            
            if(connectionManager.isConnected()) {			
            	new Backup().execute();
    		}
    		else {
    			Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
    		}
        } catch(IOException e) {
        	e.printStackTrace();
        }
	}//Backup
	
	
	private class Backup extends AsyncTask<Void, Void, Void> {
		JSONObject jObj;
		ProgressDialog pDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait....");
            pDialog.setCancelable(false);
            pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("email", editBackup.getText().toString()));
            param.add(new BasicNameValuePair("upload_file", ""));

            try {
                jObj = new JSONParser().getJSONFromUrl(CommonConstants.BACKUP, param, CommonConstants.POST);
            } catch (JSONException e) {
                e.printStackTrace();
            }
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			try {
				int responseCode = jObj.getInt("statusCode");
				int status = jObj.getInt("status");				
				
				if(status == 1 && responseCode == 200){
					
				}
				else if(status == 2 && responseCode == 401) {
					String message = jObj.getString("message");
					Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				}
				else if(status == 3 && responseCode == 401) {
					String message = jObj.getString("message");
					Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
		}
	}//Backup

}
