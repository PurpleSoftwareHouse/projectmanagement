package com.su.pm;

import java.text.SimpleDateFormat;
import java.util.List;

import com.su.pm.models.Borrow;
import com.su.pm.models.Lend;
import com.su.pm.models.Project;
import com.su.pm.models.Transaction;
import com.su.pm.util.BaseActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LeBo_TransactionActivity extends BaseActivity {
	Context context = this;

	TextView txtDate, valueDate;
	TextView valuePayment;
	TextView valueAmount;
	TextView valueCredit;
	TextView valueReason;
	TextView valueName;
	TextView valuePhone;
	Button btnEdit, btnDelete, btnAddMore;
	LinearLayout transLayout;
	
	Project project;
	String type, back;
	List<Transaction> trans;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_le_bo_transaction);
		
		valueDate = (TextView) findViewById(R.id.txt_date_value);
		valuePayment = (TextView) findViewById(R.id.payment_value);
		valueAmount = (TextView) findViewById(R.id.amount_value);
		valueCredit = (TextView) findViewById(R.id.credit_value);
		valueReason = (TextView) findViewById(R.id.reason_value);
		valueName = (TextView) findViewById(R.id.name_value1);
		valuePhone = (TextView) findViewById(R.id.phone_value);
		btnEdit = (Button) findViewById(R.id.edit);
		btnDelete = (Button) findViewById(R.id.delete);
		btnAddMore = (Button) findViewById(R.id.add_more1);
		transLayout = (LinearLayout) findViewById(R.id.trans_layout);

		project = (Project) getIntent().getSerializableExtra("Project");
		back = getIntent().getStringExtra("Back");
		type = project.type;
		
		//set value
		if (type.equals("Lend")) {
			setTitleBar(true, true, R.string.lend_transaction_detail); 
		}
		else if (type.equals("Borrow")) {
			setTitleBar(true, true, R.string.borrow_transaction_detail); 
		}
		
		String txtDate = java.text.DateFormat.getDateInstance().format(project.project_date);
		valueDate.setText(txtDate);
		valuePayment.setText(project.payment);
		valueAmount.setText(project.amount);
		valueCredit.setText(project.credit);
		valueReason.setText(project.reason);
		valueName.setText(project.project_name);
		valuePhone.setText(project.phone);
		
		//add other transaction
		addTransaction();
		
		btnEdit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, TransactionEditActivity.class);
				intent.putExtra("Project", project); 
				startActivity(intent);
			}
		});
		
		btnDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage("Are you sure you want to delete this transaction ?")
				       .setCancelable(false)
				       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				                //do things
				        	   dialog.dismiss();
				        	   Double len = Double.valueOf(pref.getLEND());
				        	   Double bor = Double.valueOf(pref.getBORROW());
				        	   Double amt = Double.valueOf(project.amount);

				        	   if (type.equals("Lend")) {
				        		    len = len - amt;
				        		    pref.setLEND(String.valueOf(len));
				        		    pref.setBALANCE(String.valueOf(len - bor));
				        		   
									Lend.delete(Lend.class, project.id);
									Transaction.transDelete(project.id, "Lend");
				        	   }
				        	   else if (type.equals("Borrow")){
				        		    bor = bor - amt;
				        		    pref.setBORROW(String.valueOf(bor));
				        		    pref.setBALANCE(String.valueOf(len - bor));
				        		    
									Borrow.delete(Borrow.class, project.id);
									Transaction.transDelete(project.id, "Borrow");
				        	   }
								
								Intent intent = new Intent(context, LeBo_ProjectDetailActivity.class);
								startActivity(intent);
				           }
				       })
				       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				                //do things
				        	   dialog.dismiss();
				           }
				       });
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
		
		btnAddMore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, AddTransactionActivity.class);
				intent.putExtra("Project", project); 
				startActivity(intent);
			}
		});
		
	}//onCreate

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		/*Intent intent = new Intent(context, LeBo_ProjectDetailActivity.class);
		startActivity(intent);*/
		if (back.equals("detail")) {
			Intent intent = new Intent(context, LeBo_ProjectDetailActivity.class);
			startActivity(intent);
		}
		else {
			Intent intent = new Intent(context, AccountDetailActivity.class);
			intent.putExtra("AccountName", project.payment);
			startActivity(intent);
		}
	}

	private void addTransaction() {
		// TODO Auto-generated method stub
		LayoutInflater inflator = getLayoutInflater();
		ViewGroup headerLayout = (ViewGroup) inflator.inflate(R.layout.transaction_hearder, transLayout, false);
		transLayout.addView(headerLayout);
		
		trans = Transaction.getTransactionById(project.id, project.type);

		TextView transDate, transAmount, transAccount;
		String dd;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
		
		for(Transaction tran : trans) {
			ViewGroup layout = (ViewGroup) inflator.inflate(R.layout.transaction_layout, transLayout, false);
			transDate = (TextView) layout.findViewById(R.id.txt_date_value);
			transAmount = (TextView) layout.findViewById(R.id.amount_value);
			transAccount = (TextView) layout.findViewById(R.id.account_value);
			
			dd = sdf.format(tran.trans_date);
			transDate.setText(dd);
			transAmount.setText(tran.amount);
			transAccount.setText(tran.payment);
			
			transLayout.addView(layout);
		}
		
	}//addTransaction
}
