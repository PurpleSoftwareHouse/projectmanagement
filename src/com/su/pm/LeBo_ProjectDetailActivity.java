package com.su.pm;

/*
 * @author SuThwe
 * @date 7 March 2016
 * */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.su.pm.models.Borrow;
import com.su.pm.models.Income;
import com.su.pm.models.Lend;
import com.su.pm.models.Project;
import com.su.pm.util.BaseActivity;
import com.su.pm.util.Preferences;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class LeBo_ProjectDetailActivity extends BaseActivity implements OnItemSelectedListener {
	Context context = this;
	
	TextView txtProjectName;
	TextView txtBalance;
	TextView txtIncome;
	TextView txtExpense;
	TextView spStartDate;
	TextView spEndDate;
	Spinner spTitle;
	Spinner spType;
	ListView listView;
	TextView txtTotal;
	TextView txtNoData;
	
	CustomList customList;
	String name;
	String balance;
	String income;
	String expense;
	Date StartDate = null, EndDate = null;
	String[] stTitle, stPayment;
	TextView txtDate;
	ArrayList<Project> projectList;
	Preferences pref;
	ArrayAdapter<String> titleAdapter;//for title spinner
	
	//datePicker
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener datePicker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_project_detail);

		listView = (ListView) findViewById(R.id.project_detail_listview);
		setTitleBar(true, true, R.string.lend_borrow);  

		pref = Preferences.getInstance(context);
		name = pref.getPROJECT_NAME();
		balance = pref.getBALANCE();
		income = pref.getLEND();
		expense = pref.getBORROW();

		//add header view
		listView.addHeaderView(addHeaderView(), null, false);

		//get Income project when activity start 
		projectList = (ArrayList<Project>) Income.getIncomesAsProject(name);
		customList = new CustomList(context, R.layout.le_bo_summary_row, projectList);
		listView.setAdapter(customList);
		
		//if no data, remove listview and show no data text
		if (projectList.size() <= 0) {
			txtNoData.setVisibility(View.VISIBLE);
		}
		else {
			calculateAmount(); 
		}
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, LeBo_TransactionActivity.class);
				intent.putExtra("Project", projectList.get(pos-1));
				startActivity(intent);
			}
		});

	   	//Date Listener
		datePicker = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				
				Date date = myCalendar.getTime();
				String calDate = java.text.DateFormat.getDateInstance().format(date);
				
				if (txtDate.getId() == spStartDate.getId()) {
					StartDate = date;
				}
				else if(txtDate.getId() == spEndDate.getId()){
					EndDate = date;
				}

				txtDate.setText(calDate);
			}
		};
/*
		titleBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, LendBorrowActivity.class);
				intent.putExtra("Summary", "summary");
				startActivity(intent);
			}
		});
*/
	}//onCreate
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(context, LendBorrowActivity.class);
		intent.putExtra("Summary", "summary");
		startActivity(intent);
	}

	private ViewGroup addHeaderView() {
		// TODO Auto-generated method stub
		LayoutInflater inflator = getLayoutInflater();
		ViewGroup layout = (ViewGroup) inflator.inflate(R.layout.le_bo_summary_header, listView, false);
		
		txtProjectName = (TextView) layout.findViewById(R.id.project_name);
		txtBalance = (TextView) layout.findViewById(R.id.balance);
		txtIncome = (TextView) layout.findViewById(R.id.income);
		txtExpense = (TextView) layout.findViewById(R.id.expense);
		spStartDate = (TextView) layout.findViewById(R.id.start_date);
		spEndDate = (TextView) layout.findViewById(R.id.end_date);
		spType = (Spinner) layout.findViewById(R.id.type);
		txtTotal = (TextView) layout.findViewById(R.id.total_value);
		txtNoData = (TextView) layout.findViewById(R.id.no_data);

		txtProjectName.setText(name);
		txtBalance.setText(getResources().getString(R.string.balance)+ "  " + balance);
		txtIncome.setText(getResources().getString(R.string.lend)+ "   " + income);
		txtExpense.setText(getResources().getString(R.string.borrow)+ "  " + expense);
		
		String[] type = {
							getResources().getString(R.string.lend), 
							getResources().getString(R.string.borrow) 
						};
		
		ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, type);
		spType.setAdapter(typeAdapter);
		
		spStartDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				txtDate = spStartDate;
				
				DatePickerDialog dateDialog = new DatePickerDialog(context,
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
				
				dateDialog.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						//call filter after select date
						FilterProject();
					}
				});
			}
		});

		spEndDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				txtDate = spEndDate;
				
				DatePickerDialog dateDialog = new DatePickerDialog(context,
						datePicker, 
						myCalendar.get(Calendar.YEAR), 
						myCalendar.get(Calendar.MONTH), 
						myCalendar.get(Calendar.DAY_OF_MONTH));
				
				dateDialog.show();
				
				dateDialog.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						//call filter after select date
						FilterProject();
					}
				});
			}
		});

		spType.setOnItemSelectedListener(this);

		return layout;
		
	}//addHeaderView 

	private class CustomList extends ArrayAdapter<Project> {
		
		Context context;
		ArrayList<Project> projectList;

		private CustomList(Context c, int resource, ArrayList<Project> projectList) {
			super(c, R.layout.le_bo_summary_row, projectList);
			this.context = c;
			this.projectList = projectList;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflater.inflate(R.layout.le_bo_summary_row, parent, false);
			
			TextView txtDate = (TextView) row.findViewById(R.id.txt_date);
			TextView txtAmount = (TextView) row.findViewById(R.id.amount);
			TextView txtPaid = (TextView) row.findViewById(R.id.paid);
			TextView txtCredit = (TextView) row.findViewById(R.id.credit);
			
			Project project = projectList.get(position);
			String dd = java.text.DateFormat.getDateInstance().format(project.project_date);
			txtDate.setText(dd);
			txtAmount.setText(project.amount);
			txtPaid.setText(project.paid);
			txtCredit.setText(project.credit);
			
			return row;
		}
		
	}//CustomList
	
	private void FilterProject() {
		// TODO Auto-generated method stub
		List<Project> proList = new ArrayList<Project>();
		if (spType.getSelectedItemPosition() == 0) {
			proList = (ArrayList<Project>) Lend.getLendByName(StartDate, EndDate, name);
		}
		else {
			proList = (ArrayList<Project>) Borrow.getBorrowByName(StartDate, EndDate, name);
		}
		
		projectList.clear();
		projectList.addAll(proList);
		
		if (projectList.size() <= 0) {
			txtNoData.setVisibility(View.VISIBLE);
			txtTotal.setText("0.0");
		}
		else {
			txtNoData.setVisibility(View.GONE);
			calculateAmount();
		}

		customList.notifyDataSetChanged();

	}//FilterProject

	private void calculateAmount() {
		// TODO Auto-generated method stub
		double paid_amt = 0.0;
		for(Project pro : projectList) {
			paid_amt += Double.valueOf(pro.paid);
		}
		
		txtTotal.setText(paid_amt + " MMK");
	}//calculateAmount

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		FilterProject();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
